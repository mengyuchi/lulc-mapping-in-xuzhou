# coding: utf-8

import numpy as np
from skimage import data
import rasterio
from matplotlib import pyplot as plt
#from fast_glcm import fast_glcm_mean
import fast_glcm

def main():
    pass


if __name__ == '__main__':
    main()

    # img = data.camera()
    file_path = 'D:/DATA/LULC/01_Prepare/Sentinel2/01_SNAP_Preprocess/L2A_Mosaicing/S2A_MSIL2A_20210921T025551_N9999_R032_T50_20221119T225446_super_resolved_mosaic.tif'
    img = np.float32(rasterio.open(file_path).read(1))

    h,w = img.shape
    ymax = 255
    ymin = 0
    xmax = max(map(max,img))
    xmin = min(map(min,img))

    for i in range(h):
        for j in range(w):
            img[i][j] = round(((ymax-ymin)*(img[i][j]-xmin)/(xmax-xmin))+ymin)

    plt.imshow(img)
    plt.tight_layout()
    plt.show()
    
    glcm_mean = fast_glcm.fast_glcm_mean(img)


    plt.imshow(glcm_mean)
    plt.tight_layout()
    plt.savefig('./XML4LULC/GLCM/img/output1.tif')
    plt.show()