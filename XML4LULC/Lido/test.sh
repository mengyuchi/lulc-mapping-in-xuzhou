#!/bin/bash -l
#SBATCH --partition=short
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=3
#SBATCH --time=2:00
#SBATCH --mem-per-cpu=100
#SBATCH --job-name=demoscript
#SBATCH --output=/work/smyumeng/LULC/scripts/batch/DL/demo.out.txt
#SBATCH --constraint=cgpu01
srun echo "START SLURM_JOB_ID $SLURM_JOB_ID (SLURM_TASK_PID
↪ $SLURM_TASK_PID) on $SLURMD_NODENAME"
srun echo "STOP on $SLURMD_NODENAME"