# *********************************************************************
# Codes for Lulc classification using Machine Learning and Deep Learning
# Based on Sentinel-2 A dataset
# Copyright: Yuchi
# Date: 2023/03/05
# *********************************************************************

# *********************************************************************
# Import Libraries
# *********************************************************************

import tifffile as tif
from PIL import Image
import rasterio
import matplotlib.pyplot as plt
import numpy as np
import os
import geopandas
from osgeo import ogr, gdal
from copy import deepcopy
import gc

import os # 方法用于改变当前工作目录到指定的路径

import cv2
import numpy as np
from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch

import this_functions

from rasterio.windows import Window 

gc.enable()

# *********************************************************************
# Working dir
# *********************************************************************

# os.chdir('/content/drive/MyDrive/LCLU-deep learning')
os.chdir('/content/LULC/ML/RF')

# *********************************************************************
# Loading data
# *********************************************************************

# Ground Truth Data
LC_io = rasterio.open('D:/DATA/LULC/02_Training/01_test/clip_test_ground_truth.tif')
LC = np.float32( LC_io.read(1) )

# Fullstack sentinel data
fullstack_io = rasterio.open('D:/DATA/LULC/01_Prepare/Sentinel2/06_test_dataset/test_S2A_MSIL2A_20210921T025551_N9999_R032_T50_20221119T225446_super_resolved_mosaic_stack_DEM_slope_collocation.tif')
fullstack = np.float16( fullstack_io.read() )

# GLCM data

pca_glcm_io = rasterio.open('D:/DATA/LULC/01_Prepare/Sentinel2/06_test_dataset/clip_test_PCA_GLCM_fullstack_reproject.tif')
pca_glcm = np.float16( pca_glcm_io.read() )


# Labdels for classification
axlabels = ['unclassified', 'Water', 'Trees', '', 'Flooded vegetation', 'Crops', '', 'Built Area','Bare ground', 'Snow/Ice', 'Clouds', 'Rangeland']



# *********************************************************************
# Color Map
# *********************************************************************
# Colormap

from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch
# from xml4lulc_functions import mem

Cmaplist = [(3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            (213/255,238/255,178/255,1), # Flooded vegetations
            (253/255,133/255,19/255,1), # crops
            (43/255,131/255,186/255,1), # Build area
            (206/255,224/255,196/255,1), # Bare Ground
            (34/255,139/255,34/255,1)]

RF_Cmaplist = [(1,1,1,1), # Nan
            (3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            # (213/255,238/255,178/255,1), 
            (253/255,133/255,19/255,1), # Flooded vegetations
            (43/255,131/255,186/255,1), # crops
            (206/255,224/255,196/255,1), # Build area 
            (34/255,139/255,34/255,1)] # Bare Ground

cmap=ListedColormap(Cmaplist)
legend_patch=[Patch(color=icolor,label=label) for icolor, label in zip(Cmaplist,axlabels)]

# plt.figure(figsize=(15,15),dpi=150)
# plt.imshow(LC, cmap=cmap)
# plt.legend(handles=legend_patch, loc='upper left')
# mem()

# *********************************************************************
# Color Map
# *********************************************************************

# LC: Ground Truch Data - Remove 'unclassified', 'Snow/Ice', 'Clouds' and 'Rangeland'

LC[LC == 0] = np.nan
LC[LC == 3] = np.nan
LC[LC == 6] = np.nan
LC[LC == 9] = np.nan
LC[LC == 10] = np.nan

# LC[LC > 0] -= 1
LC[LC > 3] -= 1
LC[LC > 5] -= 1
LC[LC > 7] -= 1
LC[LC > 7] -= 1

axlabels = ['Water', 'Forest', 'Flooded vegetation', 'Crops', 'Built Area','Bare ground', 'Rangeland']

gc.collect()
np.unique(LC)[:8]
# Return a new array of given shape and type, filled with ones.
kernel = np.ones( (2,2),np.uint8 )

# LC2: Remove ground truth data edge pixels
# LC = np.float32( LC_io.read(1) )
LC2 = np.zeros( np.shape(LC) )
for ii in range( 1, int(np.nanmax(LC))+1 ):
  single_class_layer = np.zeros( np.shape(LC) )
  single_class_layer[LC == ii] = ii
  single_class_layer = cv2.erode( single_class_layer, kernel, iterations = 1 )
  # 参数说明：src表示的是输入图片，kernel表示的是方框的大小，iteration表示迭代的次数
  # 腐蚀操作原理：存在一个kernel，比如(2, 2)，在图像中不断的平移，在这个9方框中，哪一种颜色所占的比重大，9个方格中将都是这种颜色
  LC2 += single_class_layer
LC2[LC2==0]=np.nan

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# transpose
# 求矩阵的转置
fullstack=np.transpose(fullstack,(1,2,0))

# normalize
# Min-Max Normalization 最小-最大值标准化

fullstack -= np.min(fullstack,axis=(0,1))
fullstack /= np.max(fullstack,axis=(0,1))

# set np.save() 路径
os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/')
np.save('fullStack_float16_normalized', fullstack)
# fullstack=np.float16( fullstack )

# *************************************************************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *************************************************************************
# fullstack = np.load('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/fullStack_float16_normalized.npy')

# 2d array
dime = np.shape(fullstack)
# fullstack_2d = np.float16( np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' ) )

# np.ravel() Return a contiguous flattened array. 让多维数组变成一维数组 
fullstack_2d = np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' )

gc.collect()


# *********************************************************************
# GLCM
# *********************************************************************

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# transpose
# 求矩阵的转置
pca_glcm=np.transpose(pca_glcm,(1,2,0))

# normalize
# Min-Max Normalization 最小-最大值标准化

pca_glcm -= np.min(pca_glcm,axis=(0,1))
pca_glcm /= np.max(pca_glcm,axis=(0,1))

# set np.save() 路径
os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/')
np.save('pca_glcm_float16_normalized_reproject', pca_glcm)

# *****************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *****************************
# pca_glcm = np.load('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/pca_glcm_float16_normalized_reproject.npy')

# 2d array
dime = np.shape(pca_glcm)
pca_glcm_2d = np.reshape( np.ravel(pca_glcm, order='F'), (dime[0]*dime[1], dime[2]), order='F' )
print(pca_glcm.shape)


gc.collect()

# *********************************************************************
# Spliting test and train samples
# *********************************************************************
gt_1d0 = np.int32( np.ravel(LC, order='F') ); gt_1d0[gt_1d0<1]=0
gt_1d = np.int32( np.ravel(LC2, order='F') ); gt_1d[gt_1d<1]=0

from this_functions import tr_te_sample

tr_samples=1000
# tr_samples=.20
val_samples=10000
tr_label, val_label, _, tr_idx, val_idx, _ = tr_te_sample( gt_1d, tr_samples, val_samples )

print('Training samples: ', str(len(tr_idx)))
print('Validation samples: ', str(len(val_idx)))
gc.collect()

np.unique(tr_label)

# *********************************************************************
# Remove some classes
# *********************************************************************

# Omitting label 3: Flooded vegetations

tr_idx = np.delete( tr_idx, np.where(tr_label==3) )
val_idx = np.delete( val_idx, np.where(val_label==3) )
tr_label = np.delete( tr_label, np.where(tr_label==3) )
val_label = np.delete( val_label, np.where(val_label==3) )
gt_1d[gt_1d==3]=0
gt_1d0[gt_1d0==3]=0

tr_label[tr_label>3] -= 1
val_label[val_label>3] -= 1
gt_1d[gt_1d>3] -= 1
gt_1d0[gt_1d0>3] -= 1

np.unique(tr_label)

# *********************************************************************
# Preprocessing
# *********************************************************************

train = fullstack_2d[tr_idx]
validation = fullstack_2d[val_idx]

pca_glcm_2d = np.nan_to_num(pca_glcm_2d)

  # GLCM Scenarios
train = np.nan_to_num(train)
validation = np.nan_to_num(validation)

gc.collect()


# *********************************************************************
#
# Deep Learining
#
# *********************************************************************

# *********************************************************************
# Import Deep libraries
# *********************************************************************
import tensorflow as tf
import keras
import time
from tensorflow import keras
from tensorflow.python.keras import regularizers
# from keras.layers.convolutional import Conv2D, Conv3D 
from tensorflow.python.keras.models import Model,Sequential
from keras.layers import Input, Dense, Flatten, Dropout,Concatenate, LSTM, GRU, SimpleRNN, Reshape, MaxPool2D,AveragePooling3D, MaxPooling3D,MaxPooling2D, Conv2D, Conv3D, Bidirectional, TimeDistributed
from keras.layers.normalization.batch_normalization import BatchNormalization
from keras.layers import CuDNNLSTM,CuDNNGRU
from tensorflow.python.keras.backend import squeeze
from keras.constraints import maxnorm
from keras.optimizers import SGD, Adam, RMSprop, Adagrad, Adadelta, Adamax, Nadam
from tensorflow.python.keras.regularizers import l2
#from tensorflow.python.keras.layers import Dense, Dropout, Flatten, LSTM, GRU, SimpleRNN, Bidirectional, Reshape
# from tensorflow.python.keras.layers import Dense

from tensorflow.python.keras.layers import Activation, ReLU, Add
# from keras.layers.convolutional import MaxPooling2D
# from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.utils import plot_model
# from keras_sequential_ascii import sequential_model_to_ascii_printout
from tensorflow.python.keras import backend as K
#if K.backend()=='tensorflow':
 #   K.set_image_dim_ordering("th")

# K.set_image_data_format('channels_first') # set format
K.set_image_data_format('channels_last') # set format

# GPU
device_name = tf.test.gpu_device_name()
if device_name != '/device:GPU:0':
  raise SystemError('GPU device not found')
print('Found GPU at:{}'.format(device_name))

# Functions
def patchgen (data, idx, p=17, verbose=0):
  #print('Based on gt_1d = GT.flatten ordering!!!')
  zpad = int(p/2)
  #Z = np.zeros([data.shape[0]+2*zpad, data.shape[1]+2*zpad,data.shape[2]])
  #for i in range (data.shape[2]):
  #  Z[:,:,i]=np.pad(data[:,:,i], ((zpad, zpad), (zpad, zpad)), 'constant')
  patch = np.zeros([idx.shape[0],data.shape[2],p,p])
  if verbose ==1:
    #print('Zero Padded data shape:',Z.shape)
    print('patch shape:',patch.shape)
  for i in range(idx.shape[0]):
    # if gt_1d = GT.flatten():
    # c = int(tr_idx[0]%data.shape[1])
    # r = int(tr_idx[0]/data.shape[1])

    # if gt_1d = np.ravel(GT, order='F'):
    r = int(idx[i]%data.shape[0])
    c = int(idx[i]/data.shape[0])

    #print(r,c)
  #1
    if r > int(p/2) and r < int(data.shape[0])-int(p/2) and c > int(p/2) and c < int(data.shape[1])-int(p/2):
      patch[i,:,:,:] = np.transpose(np.expand_dims(data[r-int(p/2):r+int(p/2)+1, c-int(p/2):c+int(p/2)+1,:], axis=0), (0,3,1,2))
  #2
    elif r <= int(p/2) and c <= int(p/2):
      patch[i, :, int(p/2)-r:, int(p/2)-c:] = np.transpose(np.expand_dims(data[:r+int(p/2)+1, :c+int(p/2)+1, :], axis=0), (0,3,1,2))
  #3
    elif r <= int(p/2) and c >= data.shape[1]-int(p/2):
      patch[i, :, int(p/2)-r:,:int(p/2)+data.shape[1]-c] = np.transpose(np.expand_dims(data[:r+int(p/2)+1, c-int(p/2):, :], axis=0), (0,3,1,2))
  #4
    elif r >= data.shape[0]-int(p/2) and c <= int(p/2):
      patch[i, :, :int(p/2)+data.shape[0]-r ,int(p/2)-c:] = np.transpose(np.expand_dims(data[r-int(p/2): ,:int(p/2)+c+1, :], axis=0), (0,3,1,2))
  #5
    elif r >= data.shape[0]-int(p/2) and c >= data.shape[1]-int(p/2):
      patch[i, :, :int(p/2)+data.shape[0]-r ,:int(p/2)+data.shape[1]-c] = np.transpose(np.expand_dims(data[r-int(p/2): ,c-int(p/2):, :], axis=0), (0,3,1,2))
  #6
    elif r <= int(p/2):
      patch[i, :, int(p/2)-r: , :] = np.transpose(np.expand_dims(data[:r+int(p/2)+1 ,c-int(p/2):c+int(p/2)+1, :], axis=0), (0,3,1,2))
  #7
    elif c >= data.shape[1]-int(p/2):
      patch[i, :, :,:int(p/2)+data.shape[1]-c ] = np.transpose(np.expand_dims(data[r-int(p/2):r+int(p/2)+1 ,c-int(p/2):, :], axis=0), (0,3,1,2))
  #8
    elif r >= data.shape[0]-int(p/2):
      patch[i, : , :int(p/2)+data.shape[0]-r, :] = np.transpose(np.expand_dims(data[r-int(p/2):, c-int(p/2):c+int(p/2)+1, :], axis=0), (0,3,1,2))
  #9
    elif c <= int(p/2):
      patch[i, :, :, int(p/2)-c:] = np.transpose(np.expand_dims(data[r-int(p/2):r+int(p/2)+1, :c+int(p/2)+1:, :], axis=0), (0,3,1,2))
    
    #import dask.array as da
    #patch = da.from_array(patch, chunks=(100))
    # gc.collect()
  return np.float16(patch)

class Custom_BatchData_Generator(tf.keras.utils.Sequence) : # Ben's version
  # https://medium.com/@mrgarg.rajat/training-on-large-datasets-that-dont-fit-in-memory-in-keras-60a974785d71
  import tensorflow as tf
  import gc

  '''
  Inputs:
  - Data cube (cube)
  - Patch size (p)
  - samples indices (s_idx)
  - labels
  - mini-Batch size (batch_size)
  '''

  def __init__( self, cube, p, s_idx, labels, batch_size ) :
    self.cube = cube
    self.p = p
    self.s_idx = s_idx
    self.labels = labels
    self.batch_size = batch_size


  def __len__(self) : # number of mini-batches based on the number of the required data patches
    return ( np.ceil(len(self.s_idx) / float(self.batch_size)) ).astype(np.int)
  
  
  def __getitem__(self, idx) :
    patch_idx = self.s_idx[ idx * self.batch_size : (idx+1) * self.batch_size ]
    batch_y = self.labels[ idx * self.batch_size : (idx+1) * self.batch_size ]
    # Channel_last
    batch_x = np.transpose( patchgen ( self.cube, patch_idx, p=self.p, verbose=0 ),(0,2,3,1) )
    # print(batch_x.shape)

    # gc.collect()
    # print('idx: ', idx)
    # print( ' - used memory: ', mem() )
    
    return batch_x, batch_y

p=31
# p=15
# batch_size = 32
# training_batch_generator = Custom_BatchData_Generator( fullstack[:,:,:], p, tr_idx, tr_label, batch_size )

from keras.backend import set_session
from keras.backend import clear_session
from keras.backend import get_session
import gc

# Reset Keras Session
def reset_keras():
    sess = get_session()
    clear_session()
    sess.close()
    sess = get_session()

    try:
        del classifier # this is from global space - change this as you need
    except:
        pass

    print(gc.collect()) # if it does something you should see a number as output

    # use the same config as you used to create the session
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 1
    config.gpu_options.visible_device_list = "0"
    set_session(tf.compat.v1.Session(config=config))


train_P=0
validation_P=0
gc.collect()
train_P = patchgen (fullstack[:,:,:], tr_idx, p=p, verbose=1)
validation_P = patchgen (fullstack[:,:,:], val_idx[:2000], p=p, verbose=1)
gc.collect()

# Channel_last
train_P = np.transpose( train_P,(0,2,3,1) )
validation_P = np.transpose( validation_P,(0,2,3,1) )
gc.collect()
# mem()

# One hot encoding
from tensorflow import keras
tr_label_mlp = keras.utils.to_categorical(tr_label-1)
val_label_mlp = keras.utils.to_categorical(val_label-1)

# Change here
cube_shape = [p,p,18]


# *********************************************************************
# VGG16
# *********************************************************************

model_name = 'VGG16'
def VGG16_function():
  import tensorflow as tf
  # from tensorflow.python.keras import layers, models, Input
  # https://www.tensorflow.org/api_docs/python/tf/keras/applications/VGG16
  input = tf.keras.Input(cube_shape, name='input')
  # Reshape
  input2=tf.image.resize(input, size=(64,64), method='bilinear', preserve_aspect_ratio=False)

  # VGG16 without top (FC layers)
  vgg16 = tf.keras.applications.VGG16(
      include_top=False, weights=None, input_tensor=input2, # width and height should not be smaller than 32
      input_shape=None, pooling=None, 
      # classes=8, classifier_activation='softmax'
  )

  flat = Flatten(name = 'Flatten')(vgg16.layers[-1].output)
  
  Output = Dense(np.max(tr_label), activation='softmax',
                #  kernel_regularizer=l2(0.005), activity_regularizer=l2(0.005),
                 name = 'Output')(flat)

  model = tf.keras.models.Model(inputs=input, outputs=Output)

  return model
model=VGG16_function()


# *********************************************************************
# ResNet50
# *********************************************************************

model_name = 'ResNet50'
def ResNet50_function(input_shape):
  input = tf.keras.Input(input_shape, name='input')
  # Reshape
  input2=tf.image.resize(input, size=(64,64), method='bilinear', preserve_aspect_ratio=False)

  # res50 without top (FC layers)
  res50 = tf.keras.applications.ResNet50(
      include_top=False, weights=None, input_tensor=input2, # width and height should be no smaller than 32
      input_shape=None, pooling=None, 
      # classes=8, classifier_activation='softmax'
  )

  flat = Flatten(name = 'Flatten')(res50.layers[-1].output)
  #
  #
  # Output = Dense(np.max(gt_1d), activation='linear', kernel_regularizer=l2(0.005), activity_regularizer=l2(0.005),
  #                   # kernel_initializer=initializer,
  #                   name = 'Output')(flat)
  Output = Dense(np.max(tr_label), activation='softmax',
                 kernel_regularizer=l2(0.005), activity_regularizer=l2(0.005),
                 name = 'Output')(flat)

  model = tf.keras.models.Model(inputs=input, outputs=Output)

  return model
model=ResNet50_function(cube_shape)

model.summary(line_length=130)

# *********************************************************************
# Simple CNN
# *********************************************************************

def CONV2D_section(INPUT, layers_num, kernels_num = 64, kernels_shape = (3,3), strides=(1,1), padding='valid', reg=0, Name = ''):
  conv_layer = INPUT
  for ii in range(layers_num):
    conv_layer = Conv2D(kernels_num, kernels_shape, 
                        #padding='same',
                        kernel_regularizer=regularizers.l2(reg),
                        #activity_regularizer=regularizers.l2(reg),
                        activation='relu',
                        strides=strides,
                        padding=padding,
                        name = Name+'_Conv2D'+str(ii))(conv_layer)
    conv_layer = BatchNormalization(name = Name+'_BN'+str(ii))(conv_layer)
    conv_layer = Dropout(0.1, name = Name+'_DO'+str(ii))(conv_layer)
  #
  return conv_layer
  
def simple_CNN(input_shape):
  input = tf.keras.Input(input_shape, name='input')
  layers_num = 1
  CNN = CONV2D_section(input, layers_num, kernels_num = 32, kernels_shape = (3,3), strides=(1,1), padding='valid', reg=0, Name = 'Conv1')
  CNN = MaxPooling2D( (2,2) )(CNN)
  CNN = CONV2D_section(CNN, layers_num, kernels_num = 64, kernels_shape = (3,3), strides=(1,1), padding='valid', reg=0, Name = 'Conv2')
  CNN = MaxPooling2D( (2,2) )(CNN)
  CNN = CONV2D_section(CNN, layers_num, kernels_num = 128, kernels_shape = (3,3), strides=(1,1), padding='valid', reg=0, Name = 'Conv3')
  CNN = MaxPooling2D( (2,2) )(CNN)
  flat = Flatten(name = 'Flatten')(CNN)
  dense = Dense(256,activation='relu',name = 'FC')(flat)
  output = Dense(np.max(tr_label), activation='softmax', name = 'softmax')(dense)

  model = tf.keras.models.Model(inputs=input, outputs=output)
  return model

model = simple_CNN(cube_shape)
model.summary(line_length=130)

gc.collect()
# mem()

# *********************************************************************
# Hyper Parameter
# *********************************************************************
loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False)
lrate = 0.0007
epochs = 100
sgd = SGD(learning_rate=lrate, #decay=decay, 
          momentum=0.9, nesterov=False)  # sgd optimizer
adam = Adam(learning_rate=lrate, beta_1=0.9, beta_2=0.999, #epsilon=5e-04, 
            #decay=decay,
            amsgrad=False) # Default
adadelta = Adadelta(learning_rate=lrate, rho=0.90, epsilon=None)#, decay=decay)
rmsprop = RMSprop(learning_rate=lrate)

decay = lrate/epochs
nadam = Nadam(learning_rate=lrate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, 
            decay=decay
            )


# *********************************************************************
# Train Deep Model
# *********************************************************************

# model = VGG16_function()
# model = ResNet50_function(input_shape=[31,31,18])
# model shape 61 -> 18
model = simple_CNN(input_shape=[31,31,18])
# print(model_name)

# Compile Model
model.compile(loss=loss, 
              # optimizer=adam, 
              optimizer=nadam, 
             #  optimizer=sgd, 
              metrics=['accuracy'])

gc.collect()

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.set_visible_devices(gpus[0], 'GPU')
    logical_gpus = tf.config.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)

import os
os.environ["CUDA_VISIBLE_DEVICES"] = '0' #use GPU with ID=0
config = tf.compat.v1.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.5 # maximun alloc gpu50% of MEM
config.gpu_options.allow_growth = True #allocate dynamically

from tensorflow.python.keras.callbacks import ModelCheckpoint, EarlyStopping
es_monitor = 'val_accuracy'
es = EarlyStopping(monitor=es_monitor, min_delta=0, mode='auto', verbose=1, patience=20, baseline=None, restore_best_weights=True)
batch = 64 # Vgg16 needs small batch size (for ResNet: 64)
# batch = 48
# lrate = 0.001

reset_keras()

import time
tic = time.perf_counter()
Fit_Model = model.fit(
    train_P[:,:,:,:18], tr_label_mlp, validation_data=(validation_P[:,:,:,:18], val_label_mlp[:2000]), 
    # train_P, tr_label, validation_data=(validation_P, val_label[:2000]), 
    epochs=epochs, verbose=1, 
    batch_size=batch,
    callbacks=[es]
     )
toc = time.perf_counter()
print('TIME: ',(toc - tic)/epochs, ' s/epoch')
gc.collect()
# mem()


# *********************************************************************
# Save Model
# *********************************************************************

# # Path to save weights
# weights_path = '/'

# import os
# if not os.path.exists(weights_path): # check if output folder exists
#     os.makedirs(weights_path)

# # model.save_weights(weights_path+'/'+model_name+'.h5')
# model.save_weights(weights_path+'/LCLU_'+model_name+'.h5')

# *********************************************************************
# Predictions
# *********************************************************************
SCENARIOS=np.arange(9,10,1)

from keras.callbacks import ModelCheckpoint, EarlyStopping
es_monitor = 'val_accuracy'
es = EarlyStopping(monitor=es_monitor, min_delta=0, mode='auto', verbose=1, patience=20, baseline=None, restore_best_weights=True)
batch = 64 
tf.random.set_seed(3)

def ModelDotFit( Tr_P, Val_P ):
  model = ResNet50_function( input_shape=[ p,p,Tr_P.shape[-1] ] )
  model.compile( loss=loss, optimizer=nadam, metrics=['accuracy'] )
  Fit_Model = model.fit( Tr_P, tr_label_mlp, 
      validation_data =( Val_P, val_label_mlp[:len(Val_P)]), 
      epochs=epochs, verbose=1, batch_size=batch, callbacks=[es] )
  return model


def ModelPrediction(Val_inputs):
  val_predictions = np.zeros( np.shape(val_label_mlp) )
  for pp in range(100):
    start=pp*int( len(val_label)/100 ); stop=(pp+1)*int( len(val_label)/100 )
    # Patch gen
    P_ii = patchgen( Val_inputs, val_idx[start:stop], p=p )
    P_ii = np.transpose( P_ii, (0,2,3,1) )
    val_predictions[start:stop] = model.predict( P_ii )
    gc.collect()
  yval_pred = np.argmax(val_predictions,axis=1)+1# Validation Predictions
  return yval_pred

for scenario in SCENARIOS:
  print('Scenario: \n', scenario)

  if scenario == 1:
    # 1 - Just Sentinel temporal: 1-11
    reset_keras()
    model = ModelDotFit( train_P[:,:,:,:11],validation_P[:2000,:,:,:11] )
    yval_pred = ModelPrediction( fullstack[:,:,:11] )
    gc.collect()

  if scenario == 2:
    # 2 - Just Sentinel temporal & Veg: 1-16
    reset_keras()
    model = ModelDotFit( train_P[:,:,:,:16],validation_P[:2000,:,:,:16] )
    yval_pred = ModelPrediction( fullstack[:,:,:16] )
    gc.collect()
  
  if scenario == 3:
    # 3 - Just Sentinel temporal & Topo: 1-11 & 17-18
    reset_keras()
    model = ModelDotFit( np.concatenate([train_P[:,:,:,:11],train_P[:,:,:,16:18]],axis=3),
                         np.concatenate([validation_P[:2000,:,:,:11],validation_P[:2000,:,:,16:18]],axis=3) )
    yval_pred = ModelPrediction( np.concatenate([fullstack[:,:,:11],fullstack[:,:,16:18]],axis=2) )
    gc.collect()

  if scenario == 4:
    # 4 - Multi-temporal, Veg, Topography
    reset_keras()
    model = ModelDotFit( train_P,validation_P[:2000,:,:,:] )
    yval_pred = ModelPrediction( fullstack )
    gc.collect()

  '''''''''''''''''''''''''''''''''''# Validation results'''''''''''''''''''''''''''''''''''

  axlabels = ['1-Water', '2-Forest', '3-Crops', '4-Built Area','5-Bare ground', '6-Rangeland']

  CM_val = this_functions.confusion_mat(val_label, yval_pred, axlabels=axlabels, plot=True,cmap="gray_r")
  # np.save('/content/drive/MyDrive/LCLU-deep learning/Results/CM_ResNet50_Scenario '+str(scenario), CM_val)

# tf.config.experimental.set_memory_growth(gpus[0],True)
gc.collect()
# mem()

from keras.callbacks import ModelCheckpoint, EarlyStopping
es_monitor = 'val_accuracy'
es = EarlyStopping(monitor=es_monitor, min_delta=0, mode='auto', verbose=1, patience=15, baseline=None, restore_best_weights=True)
batch = 64
tf.random.set_seed(3)
epochs = 70
def ModelDotFit( training_batch_generator, validation_batch_generator, input_shape ):
  model = simple_CNN( input_shape=input_shape )
  model.compile( loss=loss, optimizer=nadam, metrics=['accuracy'] )
  Fit_Model = model.fit(
      training_batch_generator, steps_per_epoch = int( len(tr_idx) // batch),
      validation_data=validation_batch_generator, validation_steps = int( len(val_idx[:2048]) // batch ),
      epochs=epochs, verbose=1,  # batch_size=batch,
      callbacks=[es]
      )
  return model


for scenario in SCENARIOS:
  print('Scenario: \n', scenario)

  # GLCM Scenarios

  if scenario == 5:
    # 5 - Just GLCM
    training_batch_generator = Custom_BatchData_Generator( pca_glcm[:1625,:2521,:], p, tr_idx, tr_label_mlp, batch )
    validation_batch_generator = Custom_BatchData_Generator( pca_glcm[:1625,:2521,:], p, val_idx[:2048], val_label_mlp[:2048], batch )
    #
    input_shape = [ p,p,51 ]
    model = ModelDotFit( training_batch_generator,validation_batch_generator, input_shape )
    gc.collect()
    validation_batch_generator = Custom_BatchData_Generator( pca_glcm[:1625,:2521,:], p, val_idx, val_label_mlp, batch )
    yval_pred = np.argmax( model.predict(validation_batch_generator),axis=1 )+1

  if scenario == 6:
    # 6 - GLCM + Multi-temporal 
    training_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:11]],axis=2), p, tr_idx, tr_label_mlp, batch )
    validation_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:11]],axis=2), p, val_idx[:2048], val_label_mlp[:2048], batch )
    #
    input_shape = [ p,p,51+11 ]
    model = ModelDotFit( training_batch_generator,validation_batch_generator, input_shape )
    gc.collect()
    validation_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:11]],axis=2), p, val_idx, val_label_mlp, batch )
    yval_pred = np.argmax( model.predict(validation_batch_generator),axis=1 )+1

  if scenario == 7:
    # 7 - GLCM + Multi-temporal and Veg
    training_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:16]],axis=2), p, tr_idx, tr_label_mlp, batch )
    validation_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:16]],axis=2), p, val_idx[:2048], val_label_mlp[:2048], batch )
    #
    input_shape = [ p,p,51+16 ]
    model = ModelDotFit( training_batch_generator,validation_batch_generator, input_shape )
    gc.collect()
    validation_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:16]],axis=2), p, val_idx, val_label_mlp, batch )
    yval_pred = np.argmax( model.predict(validation_batch_generator),axis=1 )+1
   
  if scenario == 8:
    # 8 - GLCM + Multi-temporal and Topography
    training_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:11],fullstack[:,:,16:18]],axis=2), p, tr_idx, tr_label_mlp, batch )
    validation_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:11],fullstack[:,:,16:18]],axis=2), p, val_idx[:2048], val_label_mlp[:2048], batch )
    #
    input_shape = [ p,p,51+11+2 ]
    model = ModelDotFit( training_batch_generator,validation_batch_generator, input_shape )
    gc.collect()
    validation_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack[:,:,:11],fullstack[:,:,16:18]],axis=2), p, val_idx, val_label_mlp, batch )
    yval_pred = np.argmax( model.predict(validation_batch_generator),axis=1 )+1

  if scenario == 9:
    input_shape = [ p,p,51+18 ]
    # 9 - GLCM + Multi-temporal, Veg, and Topo
    training_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack],axis=2), p, tr_idx, tr_label_mlp, batch )
    validation_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack],axis=2), p, val_idx[:2048], val_label_mlp[:2048], batch )
    #
    model = ModelDotFit( training_batch_generator,validation_batch_generator, input_shape )
    gc.collect()
    validation_batch_generator = Custom_BatchData_Generator( np.concatenate([pca_glcm[:1625,:2521,:],fullstack],axis=2), p, val_idx, val_label_mlp, batch )
    yval_pred = np.argmax( model.predict(validation_batch_generator),axis=1 )+1

  '''''''''''''''''''''''''''''''''''# Validation results'''''''''''''''''''''''''''''''''''

  axlabels = ['1-Water', '2-Forest', '3-Crops', '4-Built Area','5-Bare ground', '6-Rangeland']

  CM_val = this_functions.confusion_mat(val_label, yval_pred, axlabels=axlabels, plot=True,cmap="gray_r")
  # np.save('/content/drive/MyDrive/LCLU-deep learning/Results/CM_Simple_CNN_Scenario '+str(scenario), CM_val)