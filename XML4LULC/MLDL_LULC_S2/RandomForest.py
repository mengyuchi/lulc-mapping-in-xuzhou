# *********************************************************************
# Codes for Lulc classification using Machine Learning and Deep Learning
# Based on Sentinel-2 A dataset
# Copyright: Yuchi
# Date: 2023/03/05
# *********************************************************************

# *********************************************************************
# Import Libraries
# *********************************************************************

import tifffile as tif
from PIL import Image
import rasterio
import matplotlib.pyplot as plt
import numpy as np
import os
import geopandas
from osgeo import ogr, gdal
from copy import deepcopy
import gc

import os # 方法用于改变当前工作目录到指定的路径

import cv2
from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch

import this_functions

from rasterio.windows import Window 

gc.enable()

# *********************************************************************
# Working dir
# *********************************************************************

# os.chdir('/content/drive/MyDrive/LCLU-deep learning')
os.chdir('D:/DATA/LULC/02_Training/03_Final_Input')

# *********************************************************************
# Loading data
# *********************************************************************

# Ground Truth Data
LC_io = rasterio.open('D:/DATA/LULC/02_Training/03_Final_Input/20210921_groundtruth_xz.tif')
LC = np.float32( LC_io.read(1) )

# Fullstack sentinel data
fullstack_io = rasterio.open('D:/DATA/LULC/02_Training/03_Final_Input/20210921_fullstack_xz.tif')
fullstack = np.float16( fullstack_io.read() )

# GLCM data

pca_glcm_io = rasterio.open('D:/DATA/LULC/02_Training/03_Final_Input/20210921_PCA_GLCM_xz.tif')
pca_glcm = np.float16( pca_glcm_io.read() )



# Labdels for classification
axlabels = ['unclassified', 'Water', 'Trees', '', 'Flooded vegetation', 'Crops', '', 'Built Area','Bare ground', 'Snow/Ice', 'Clouds', 'Rangeland']



# *********************************************************************
# Color Map
# *********************************************************************
# Colormap

from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch
# from xml4lulc_functions import mem

# Cmaplist = [(3/255,20/255,176/255,1), # water
#             (1/255,58/255,11/255,1), # Forest
#             (213/255,238/255,178/255,1), # Flooded vegetations
#             (253/255,133/255,19/255,1), # crops
#             (43/255,131/255,186/255,1), # Build area
#             (206/255,224/255,196/255,1), # Bare Ground
#             (34/255,139/255,34/255,1)]

RF_Cmaplist = [(1,1,1,1), # Nan
            (3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            # (213/255,238/255,178/255,1), 
            (253/255,133/255,19/255,1), # Flooded vegetations
            (43/255,131/255,186/255,1), # crops
            (206/255,224/255,196/255,1), # Build area 
            (34/255,139/255,34/255,1)] # Bare Ground

cmap=ListedColormap(RF_Cmaplist)
legend_patch=[Patch(color=icolor,label=label) for icolor, label in zip(RF_Cmaplist,axlabels)]

# plt.figure(figsize=(15,15),dpi=150)
# plt.imshow(LC, cmap=cmap)
# plt.legend(handles=legend_patch, loc='upper left')
# mem()

# *********************************************************************
# Color Map
# *********************************************************************

# LC: Ground Truch Data - Remove 'unclassified', 'Snow/Ice', 'Clouds' and 'Rangeland'

LC[LC == 0] = np.nan
LC[LC == 3] = np.nan
LC[LC == 6] = np.nan
LC[LC == 9] = np.nan
LC[LC == 10] = np.nan

# LC[LC > 0] -= 1
LC[LC > 3] -= 1
LC[LC > 5] -= 1
LC[LC > 7] -= 1
LC[LC > 7] -= 1

axlabels = ['Water', 'Forest', 'Flooded vegetation', 'Crops', 'Built Area', 'Bare ground', 'Rangeland']

gc.collect()
np.unique(LC)[:8]
# Return a new array of given shape and type, filled with ones.
kernel = np.ones( (2,2),np.uint8 )

# LC2: Remove ground truth data edge pixels
# LC = np.float32( LC_io.read(1) )
LC2 = np.zeros( np.shape(LC) )
for ii in range( 1, int(np.nanmax(LC))+1 ):
  single_class_layer = np.zeros( np.shape(LC) )
  single_class_layer[LC == ii] = ii
  single_class_layer = cv2.erode( single_class_layer, kernel, iterations = 1 )
  # 参数说明：src表示的是输入图片，kernel表示的是方框的大小，iteration表示迭代的次数
  # 腐蚀操作原理：存在一个kernel，比如(2, 2)，在图像中不断的平移，在这个9方框中，哪一种颜色所占的比重大，9个方格中将都是这种颜色
  LC2 += single_class_layer
LC2[LC2==0]=np.nan

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# # transpose
# # 求矩阵的转置
# fullstack=np.transpose(fullstack,(1,2,0))

# # normalize
# # Min-Max Normalization 最小-最大值标准化

# fullstack -= np.min(fullstack,axis=(0,1))
# fullstack /= np.max(fullstack,axis=(0,1))

# # set np.save() 路径
# os.chdir(r'D:/DATA/LULC/02_Training/03_Final_Input/')
# np.save('fullStack_float16_normalized', fullstack)
# # fullstack=np.float16( fullstack )

# *************************************************************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *************************************************************************
fullstack = np.load('D:/DATA/LULC/02_Training/03_Final_Input/fullStack_float16_normalized.npy')

# 2d array
dime = np.shape(fullstack)
# fullstack_2d = np.float16( np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' ) )

# np.ravel() Return a contiguous flattened array. 让多维数组变成一维数组 
fullstack_2d = np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' )

gc.collect()


# *********************************************************************
# GLCM
# *********************************************************************

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# # transpose
# # 求矩阵的转置
# pca_glcm=np.transpose(pca_glcm,(1,2,0))

# # normalize
# # Min-Max Normalization 最小-最大值标准化

# pca_glcm -= np.min(pca_glcm,axis=(0,1))
# pca_glcm /= np.max(pca_glcm,axis=(0,1))

# # set np.save() 路径
# os.chdir(r'D:/DATA/LULC/02_Training/03_Final_Input/')
# np.save('pca_glcm_float16_normalized_reproject', pca_glcm)

# *****************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *****************************
pca_glcm = np.load('D:/DATA/LULC/02_Training/03_Final_Input/pca_glcm_float16_normalized_reproject.npy')

# 2d array
dime = np.shape(pca_glcm)
pca_glcm_2d = np.reshape( np.ravel(pca_glcm, order='F'), (dime[0]*dime[1], dime[2]), order='F' )
print(pca_glcm.shape)


gc.collect()

# *********************************************************************
# Spliting test and train samples
# *********************************************************************
gt_1d0 = np.int32( np.ravel(LC, order='F') ); gt_1d0[gt_1d0<1]=0
gt_1d = np.int32( np.ravel(LC2, order='F') ); gt_1d[gt_1d<1]=0

from this_functions import tr_te_sample

tr_samples=50000000
# tr_samples=.20
val_samples=100000
tr_label, val_label, _, tr_idx, val_idx, _ = tr_te_sample( gt_1d, tr_samples, val_samples )

print('Training samples: ', str(len(tr_idx)))
print('Validation samples: ', str(len(val_idx)))
gc.collect()

np.unique(tr_label)

# *********************************************************************
# Remove some classes
# *********************************************************************

# Omitting label 3: Flooded vegetations

tr_idx = np.delete( tr_idx, np.where(tr_label==3) )
val_idx = np.delete( val_idx, np.where(val_label==3) )
tr_label = np.delete( tr_label, np.where(tr_label==3) )
val_label = np.delete( val_label, np.where(val_label==3) )
gt_1d[gt_1d==3]=0
gt_1d0[gt_1d0==3]=0

tr_label[tr_label>3] -= 1
val_label[val_label>3] -= 1
gt_1d[gt_1d>3] -= 1
gt_1d0[gt_1d0>3] -= 1

np.unique(tr_label)

# *********************************************************************
# Preprocessing
# *********************************************************************

train = fullstack_2d[tr_idx]
validation = fullstack_2d[val_idx]

gc.collect()

# *********************************************************************
# Random Forest
# *********************************************************************

# RF
"""CLASSIFIER"""
from sklearn.ensemble import RandomForestClassifier
from this_functions import confusion_mat
n_estimator_test = 800
classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test,max_depth=None)
# classifier_RF.fit(train, tr_label)

# Remove nan pixels
pca_glcm_2d = np.nan_to_num(pca_glcm_2d)

# GLCM Scenarios
train = np.nan_to_num(train)
validation = np.nan_to_num(validation)

# Scenarios (choose between 1 - 9)
SCENARIOS = np.arange(1,10)
# scenario = 9

for scenario in SCENARIOS:
  '''''''''''''''''''''''''''''''  # define the model '''''''''''''''''''''''''''''''
  if scenario == 1:
    # 1 - Just Sentinel temporal: 1-11
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test,max_depth=60)
    classifier_RF.fit( train[:,:11], tr_label )

  if scenario == 2:
    # 2 - Just Sentinel temporal & Veg: 1-17
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test)#,max_depth=60)
    classifier_RF.fit( train[:,:17], tr_label )
    
  if scenario == 3:
    # 3 - Just Sentinel temporal & Topo: 1-11 & 18-20
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test)#,max_depth=60)
    classifier_RF.fit( np.hstack( [train[:,:11], train[:,17:20]] ) , tr_label)
    
  if scenario == 4:
    # 4 - Just Sentinel temporal & Veg & Topo: 1-20
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test)#,max_depth=60)
    classifier_RF.fit( train, tr_label )


  # Remove nan pixels
  # pca_glcm_2d = np.nan_to_num(pca_glcm_2d)

  # # GLCM Scenarios
  # train = np.nan_to_num(train) 

  if scenario == 5:
    # 5 - Just GLCM
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test,max_depth=60)
    classifier_RF.fit( pca_glcm_2d[tr_idx], tr_label )

  if scenario == 6:
    # 6 - GLCM + Multi-temporal and Spectral
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test,max_depth=60)
    classifier_RF.fit( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ), tr_label )

  if scenario == 7:
    # 7 - GLCM + Multi-temporal, Spectral, and Vegetation
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test)#,max_depth=60)
    classifier_RF.fit( np.hstack( [train[:,:17], pca_glcm_2d[tr_idx]] ), tr_label )
    
  if scenario == 8:
    # 8 - GLCM + Multi-temporal, Spectral, and Topography
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test)#,max_depth=60)
    classifier_RF.fit( np.hstack( [train[:,:11], train[:,17:20], pca_glcm_2d[tr_idx]] ) , tr_label)
    
  if scenario == 9:
    # 9 - GLCM + Multi-temporal, Spectral, Vegetation, an
    # 
    # d Topography
    classifier_RF = RandomForestClassifier(n_estimators=n_estimator_test)#,max_depth=60)
    classifier_RF.fit( np.hstack( [train, pca_glcm_2d[tr_idx]] ), tr_label )

  # Prediction

  if scenario == 1:
    # 1 - Just Sentinel temporal: 1-11
    ytr_pred = classifier_RF.predict(train[:,:11])
    yval_pred = classifier_RF.predict(validation[:,:11])

  if scenario == 2:
    # 2 - Just Sentinel temporal & Veg: 1-17
    ytr_pred = classifier_RF.predict(train[:,:17])
    yval_pred = classifier_RF.predict(validation[:,:17])

  if scenario == 3:
    # 3 - Just Sentinel temporal & Topo: 1-11 & 18-20
    ytr_pred = classifier_RF.predict(np.hstack( [train[:,:11], train[:,17:20]] ))
    yval_pred = classifier_RF.predict(np.hstack( [validation[:,:11], validation[:,17:20]] ))

  if scenario == 4:
    # 4 - Just Sentinel temporal & Veg & Topo: 1-18
    ytr_pred = classifier_RF.predict(train)
    yval_pred = classifier_RF.predict(validation)

  # pca_glcm_2d = np.nan_to_num(pca_glcm_2d)
  # validation = np.nan_to_num(validation)

  if scenario == 5:
    # 5 - Just GLCM
    ytr_pred = classifier_RF.predict( pca_glcm_2d[tr_idx] )
    yval_pred = classifier_RF.predict( pca_glcm_2d[val_idx] )

  if scenario == 6:
    # 6 - GLCM + Multi-temporal and Spectral
    ytr_pred = classifier_RF.predict( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_RF.predict( np.hstack( [validation[:,:11], pca_glcm_2d[val_idx]] ) )

  if scenario == 7:
    # 7 - GLCM + Multi-temporal, Spectral, and Vegetation
    ytr_pred = classifier_RF.predict( np.hstack( [train[:,:17], pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_RF.predict( np.hstack( [validation[:,:17], pca_glcm_2d[val_idx]] ) )
    
  if scenario == 8:
    # 8 - GLCM + Multi-temporal, Spectral, and Topography
    ytr_pred = classifier_RF.predict( np.hstack( [train[:,:11], train[:,17:20], pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_RF.predict( np.hstack( [validation[:,:11], validation[:,17:20], pca_glcm_2d[val_idx]] ) )

  if scenario == 9:
    # 9 - GLCM + Multi-temporal, Spectral, Vegetation, and Topography
    ytr_pred = classifier_RF.predict( np.hstack( [train, pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_RF.predict( np.hstack( [validation, pca_glcm_2d[val_idx]] ) )


  axlabels = ['Water', 'Forest', 'Crops', 'Built Area','Bare ground', 'Rangeland']

# cm img path
  cm_tr_path_name = 'D:/DATA/LULC/05_Results/RF/cm_tr_scenario' + str(scenario) + 'nes' + str(n_estimator_test) +'.png'
  cm_val_path_name = 'D:/DATA/LULC/05_Results/RF/cm_tr_scenario' + str(scenario) + 'nes' + str(n_estimator_test) +'.png'

  print('Scenario: \n', scenario)
  print('Model:\n')
  print(classifier_RF)

  # Training results
  CM_tr = confusion_mat(tr_label, ytr_pred, cm_tr_path_name,axlabels=axlabels, plot=True,cmap="gray_r")

  # Validation results
  CM_val = confusion_mat(val_label, yval_pred, cm_val_path_name, axlabels=axlabels, plot=True,cmap="gray_r")
  # np.save('/content/drive/MyDrive/LCLU-deep learning/Results/CM_RF_Scenario '+str(scenario), CM_val)

  # *********************************************************************
  # Full Classification Map
  # *********************************************************************

# 解注释
  # # Indexs for test samples
  # te_idx = np.where(gt_1d0!=0)[0]

  # # yte_pred = classifier_RF.predict(test)


  # test = fullstack_2d[te_idx]
  # test = np.nan_to_num(test)

  # # print(np.isnan(test).any())
  # # Choose best scenario
  # yte_pred = classifier_RF.predict( np.hstack( [test, pca_glcm_2d[te_idx]] ) ) # Best scenario

  # dime = np.shape(fullstack)

  # classified_map = np.nan*np.zeros( dime[0]*dime[1] )
  # classified_map[te_idx] = yte_pred
  # classified_map[gt_1d0==0]=np.nan
  # classified_map = np.reshape( classified_map, (dime[0], dime[1]), order='F' )

  # predmap = np.uint8(classified_map)


  # RF_axlabels = ['Nan', 'Water', 'Forest', 'Crop', 'Built Area', 'Bare Ground', 'Rangeland']

  # cmap=ListedColormap(RF_Cmaplist)
  # legend_patch=[Patch(color=icolor,label=label) for icolor, label in zip(RF_Cmaplist,RF_axlabels)]


  # plt.figure(figsize=(10,10),dpi=300)
  # plt.savefig('D:/DATA/LULC/05_Results/Maps/test.png')
  # #plt.imshow(predmap, cmap=cmap);plt.grid(False)
  # plt.legend(handles=legend_patch, loc='upper left')

  # # Save geotif
  # with rasterio.open(
  #         'D:/DATA/LULC/05_Results/Maps/classified_fullmap_RF_best.tif',
  #         'w',  # writing mode
  #         driver='GTiff',
  #         width=predmap.shape[1],
  #         height=predmap.shape[0],
  #         count=1,  # Bands
  #         dtype=np.uint8,
  #         nodata=0,
  #         transform=LC_io.transform,
  #         crs=LC_io.crs  # a coordinate reference system identifier or description
  # ) as dst:
  #   dst.write( np.expand_dims(predmap, axis=0).astype(np.uint8) )