# *********************************************************************
# Codes for Lulc classification using Machine Learning and Deep Learning
# Based on Sentinel-2 A dataset
# Copyright: Yuchi
# Date: 2023/03/05
# *********************************************************************

# *********************************************************************
# Import Libraries
# *********************************************************************

import tifffile as tif
from PIL import Image
import rasterio
import matplotlib.pyplot as plt
import numpy as np
import os
import geopandas
from osgeo import ogr, gdal
from copy import deepcopy
import gc

import os # 方法用于改变当前工作目录到指定的路径

import cv2
import numpy as np
from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch

import this_functions

from rasterio.windows import Window 

gc.enable()

# *********************************************************************
# Working dir
# *********************************************************************

# os.chdir('/content/drive/MyDrive/LCLU-deep learning')
os.chdir('/content/LULC/ML/RF')

# *********************************************************************
# Loading data
# *********************************************************************

# Ground Truth Data
LC_io = rasterio.open('D:/DATA/LULC/02_Training/01_test/clip_test_ground_truth.tif')
LC = np.float32( LC_io.read(1) )

# Fullstack sentinel data
fullstack_io = rasterio.open('D:/DATA/LULC/01_Prepare/Sentinel2/06_test_dataset/test_S2A_MSIL2A_20210921T025551_N9999_R032_T50_20221119T225446_super_resolved_mosaic_stack_DEM_slope_collocation.tif')
fullstack = np.float16( fullstack_io.read() )

# GLCM data

pca_glcm_io = rasterio.open('D:/DATA/LULC/01_Prepare/Sentinel2/06_test_dataset/clip_test_PCA_GLCM_fullstack_reproject.tif')
pca_glcm = np.float16( pca_glcm_io.read() )


# Labdels for classification
axlabels = ['unclassified', 'Water', 'Trees', '', 'Flooded vegetation', 'Crops', '', 'Built Area','Bare ground', 'Snow/Ice', 'Clouds', 'Rangeland']



# *********************************************************************
# Color Map
# *********************************************************************
# Colormap

from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch
# from xml4lulc_functions import mem

Cmaplist = [(3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            (213/255,238/255,178/255,1), # Flooded vegetations
            (253/255,133/255,19/255,1), # crops
            (43/255,131/255,186/255,1), # Build area
            (206/255,224/255,196/255,1), # Bare Ground
            (34/255,139/255,34/255,1)]

RF_Cmaplist = [(1,1,1,1), # Nan
            (3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            # (213/255,238/255,178/255,1), 
            (253/255,133/255,19/255,1), # Flooded vegetations
            (43/255,131/255,186/255,1), # crops
            (206/255,224/255,196/255,1), # Build area 
            (34/255,139/255,34/255,1)] # Bare Ground

cmap=ListedColormap(Cmaplist)
legend_patch=[Patch(color=icolor,label=label) for icolor, label in zip(Cmaplist,axlabels)]

# plt.figure(figsize=(15,15),dpi=150)
# plt.imshow(LC, cmap=cmap)
# plt.legend(handles=legend_patch, loc='upper left')
# mem()

# *********************************************************************
# Color Map
# *********************************************************************

# LC: Ground Truch Data - Remove 'unclassified', 'Snow/Ice', 'Clouds' and 'Rangeland'

LC[LC == 0] = np.nan
LC[LC == 3] = np.nan
LC[LC == 6] = np.nan
LC[LC == 9] = np.nan
LC[LC == 10] = np.nan

# LC[LC > 0] -= 1
LC[LC > 3] -= 1
LC[LC > 5] -= 1
LC[LC > 7] -= 1
LC[LC > 7] -= 1

axlabels = ['Water', 'Forest', 'Flooded vegetation', 'Crops', 'Built Area','Bare ground', 'Rangeland']

gc.collect()
np.unique(LC)[:8]
# Return a new array of given shape and type, filled with ones.
kernel = np.ones( (2,2),np.uint8 )

# LC2: Remove ground truth data edge pixels
# LC = np.float32( LC_io.read(1) )
LC2 = np.zeros( np.shape(LC) )
for ii in range( 1, int(np.nanmax(LC))+1 ):
  single_class_layer = np.zeros( np.shape(LC) )
  single_class_layer[LC == ii] = ii
  single_class_layer = cv2.erode( single_class_layer, kernel, iterations = 1 )
  # 参数说明：src表示的是输入图片，kernel表示的是方框的大小，iteration表示迭代的次数
  # 腐蚀操作原理：存在一个kernel，比如(2, 2)，在图像中不断的平移，在这个9方框中，哪一种颜色所占的比重大，9个方格中将都是这种颜色
  LC2 += single_class_layer
LC2[LC2==0]=np.nan

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# transpose
# 求矩阵的转置
fullstack=np.transpose(fullstack,(1,2,0))

# normalize
# Min-Max Normalization 最小-最大值标准化

fullstack -= np.min(fullstack,axis=(0,1))
fullstack /= np.max(fullstack,axis=(0,1))

# set np.save() 路径
os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/')
np.save('fullStack_float16_normalized', fullstack)
# fullstack=np.float16( fullstack )

# *************************************************************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *************************************************************************
# fullstack = np.load('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/fullStack_float16_normalized.npy')

# 2d array
dime = np.shape(fullstack)
# fullstack_2d = np.float16( np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' ) )

# np.ravel() Return a contiguous flattened array. 让多维数组变成一维数组 
fullstack_2d = np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' )

gc.collect()


# *********************************************************************
# GLCM
# *********************************************************************

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# transpose
# 求矩阵的转置
pca_glcm=np.transpose(pca_glcm,(1,2,0))

# normalize
# Min-Max Normalization 最小-最大值标准化

pca_glcm -= np.min(pca_glcm,axis=(0,1))
pca_glcm /= np.max(pca_glcm,axis=(0,1))

# set np.save() 路径
os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/')
np.save('pca_glcm_float16_normalized_reproject', pca_glcm)

# *****************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *****************************
# pca_glcm = np.load('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/pca_glcm_float16_normalized_reproject.npy')

# 2d array
dime = np.shape(pca_glcm)
pca_glcm_2d = np.reshape( np.ravel(pca_glcm, order='F'), (dime[0]*dime[1], dime[2]), order='F' )
print(pca_glcm.shape)


gc.collect()

# *********************************************************************
# Spliting test and train samples
# *********************************************************************
gt_1d0 = np.int32( np.ravel(LC, order='F') ); gt_1d0[gt_1d0<1]=0
gt_1d = np.int32( np.ravel(LC2, order='F') ); gt_1d[gt_1d<1]=0

from this_functions import tr_te_sample

tr_samples=1000
# tr_samples=.20
val_samples=10000
tr_label, val_label, _, tr_idx, val_idx, _ = tr_te_sample( gt_1d, tr_samples, val_samples )

print('Training samples: ', str(len(tr_idx)))
print('Validation samples: ', str(len(val_idx)))
gc.collect()

np.unique(tr_label)

# *********************************************************************
# Remove some classes
# *********************************************************************

# Omitting label 3: Flooded vegetations

tr_idx = np.delete( tr_idx, np.where(tr_label==3) )
val_idx = np.delete( val_idx, np.where(val_label==3) )
tr_label = np.delete( tr_label, np.where(tr_label==3) )
val_label = np.delete( val_label, np.where(val_label==3) )
gt_1d[gt_1d==3]=0
gt_1d0[gt_1d0==3]=0

tr_label[tr_label>3] -= 1
val_label[val_label>3] -= 1
gt_1d[gt_1d>3] -= 1
gt_1d0[gt_1d0>3] -= 1

np.unique(tr_label)

# *********************************************************************
# Preprocessing
# *********************************************************************

train = fullstack_2d[tr_idx]
validation = fullstack_2d[val_idx]

gc.collect()

# *********************************************************************
# SVM
# *********************************************************************
SCENARIOS=np.arange(9,10,1)

# For different layers (scenarios)
from sklearn.svm import SVC
# from SVM_with_Optimization import SVM_Classifier
import warnings
warnings.filterwarnings("ignore")


# C_settings = [1, 10, 7] # [c1, c2, samples between c1 and c2] | c = 10**c1:10**c2
# G_settings = [-5, 10, 7] # [gamma1, gamma2, samples between gamma1 and gamma2] | gamma = 10**gamma1:10**gamma2
c = 1.0 # # SVM regularization parameter
gamma = 20
degree = 3 
rounds = 5
svm_kernels = ['rbf']
es = 20


for scenario in SCENARIOS:
  '''''''''''''''''''''''''''''''  # define the model '''''''''''''''''''''''''''''''
  classifier_SVM = SVC(C=c,kernel='rbf',degree=3, gamma='scale',max_iter=50000, decision_function_shape='ovr',probability = False)
  # classifier_SVM = SVC(C=c, kernel=Kernel, gamma=gamma, decision_function_shape='ovr', max_iter=50000)
  if scenario == 1:
    # 1 - Just Sentinel temporal: 1-11
    classifier_SVM.fit(train[:,:11], tr_label)

  if scenario == 2:
    # 2 - Just Sentinel temporal & Veg: 1-16
    classifier_SVM.fit(train[:,:16], tr_label)

  if scenario == 3:
    # 3 - Just Sentinel temporal & Topo: 1-11 & 17-18
    classifier_SVM.fit(np.hstack( [train[:,:11], train[:,16:18]] ), tr_label)

  if scenario == 4:
    # 4 - Multi-temporal, Spectral, Veg, and Topo
    classifier_SVM.fit(np.hstack( train, tr_label))

  # GLCM Scenarios

  if scenario == 5:
    # 5 - Just GLCM
    classifier_SVM.fit(pca_glcm_2d[tr_idx], tr_label)

  if scenario == 6:
    # 6 - GLCM + Multi-temporal and Spectral
    classifier_SVM.fit( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ), tr_label)

  if scenario == 7:
    # 7 - GLCM + Multi-temporal, Spectral, and Veg
    classifier_SVM.fit(np.hstack( [train[:,:16], pca_glcm_2d[tr_idx]] ))
   
  if scenario == 8:
    # 8 - GLCM + Multi-temporal, Spectral, and Topography
    classifier_SVM.fit(np.hstack( [train[:,:11], train[:,16:18], pca_glcm_2d[tr_idx]] ))

  if scenario == 9:
    # 9 - GLCM + Multi-temporal, Spectral, Veg, and Topo
    classifier_SVM.fit(np.hstack( [train, pca_glcm_2d[tr_idx]] ), tr_label)


  '''''''''''''''''''# Prediction for different layers of fullstack data (scenarios)'''''''''''''''''''

  if scenario == 1:
    # 1 - Just Sentinel temporal: 1-11
    ytr_pred = classifier_SVM.predict(train[:,:11])
    yval_pred = classifier_SVM.predict(validation[:,:11])

  if scenario == 2:
    # 2 - Just Sentinel temporal & Veg: 1-16
    ytr_pred = classifier_SVM.predict(train[:,:16])
    yval_pred = classifier_SVM.predict(validation[:,:16])

  if scenario == 3:
    # 3 - Just Sentinel temporal & Topo: 1-11 & 17-18
    ytr_pred = classifier_SVM.predict(np.hstack( [train[:,:11], train[:,16:18]] ))
    yval_pred = classifier_SVM.predict(np.hstack( [validation[:,:11], validation[:,16:18]] ))

  if scenario == 4:
    # 4 - Multi-temporal, Spectral, Veg, and Topo
    ytr_pred = classifier_SVM.predict(train)
    yval_pred = classifier_SVM.predict(validation)

  # GLCM Scenarios

  if scenario == 5:
    # 5 - Just GLCM
    ytr_pred = classifier_SVM.predict(pca_glcm_2d[tr_idx])
    yval_pred = classifier_SVM.predict( pca_glcm_2d[val_idx] )

  if scenario == 6:
    # 6 - GLCM + Multi-temporal and Spectral
    yval_pred = classifier_SVM.predict( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_SVM.predict( np.hstack( [validation[:,:11], pca_glcm_2d[val_idx]] ) )

  if scenario == 7:
    # 7 - GLCM + Multi-temporal, Spectral, and Veg
    ytr_pred = classifier_SVM.predict( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_SVM.predict( np.hstack( [validation[:,:11], pca_glcm_2d[val_idx]] ) )
    
  if scenario == 8:
    # 8 - GLCM + Multi-temporal, Spectral, and Topography
    ytr_pred = classifier_SVM.predict( np.hstack( [train[:,:11], train[:,16:18], pca_glcm_2d[val_idx]] ) )
    yval_pred = classifier_SVM.predict( np.hstack( [validation[:,:11], validation[:,16:18], pca_glcm_2d[val_idx]] ) )

  if scenario == 9:
    # 9 - GLCM + Multi-temporal, Spectral, Veg, and Topo
    ytr_pred = classifier_SVM.predict( np.hstack( [train, pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_SVM.predict( np.hstack( [validation, pca_glcm_2d[val_idx]] ) )

  '''''''''''''''''''''''''''''''''''# Validation results'''''''''''''''''''''''''''''''''''

  # axlabels = ['1-Agriculture','3-Deciduous Trees','5-NonVegetated Open Land','6-Water','7-Wetland', '8-High Intensity Urban', '9-Low Intensity Urban', '10-Industrial']
  axlabels = ['1-Water', '2-Forest', '3-Crops', '4-Built Area','5-Bare ground', '6-Rangeland']


  print('Scenario: \n', scenario)
  print('Model:\n')
  print(classifier_SVM)
  CM_val = this_functions.confusion_mat(val_label, yval_pred, axlabels=axlabels, plot=True,cmap="gray_r")
  # np.save('/content/drive/MyDrive/LCLU-deep learning/Results/CM_SVM_Scenario '+str(scenario), CM_val)