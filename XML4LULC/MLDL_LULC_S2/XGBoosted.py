# *********************************************************************
# Codes for Lulc classification using Machine Learning and Deep Learning
# Based on Sentinel-2 A dataset
# Copyright: Yuchi
# Date: 2023/03/05
# *********************************************************************

# *********************************************************************
# Import Libraries
# *********************************************************************

import tifffile as tif
from PIL import Image
import rasterio
import matplotlib.pyplot as plt
import numpy as np
import os
import geopandas
from osgeo import ogr, gdal
from copy import deepcopy
import gc

import os # 方法用于改变当前工作目录到指定的路径

import cv2
import numpy as np
from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch

import this_functions

from rasterio.windows import Window 

gc.enable()

# *********************************************************************
# Working dir
# *********************************************************************

# os.chdir('/content/drive/MyDrive/LCLU-deep learning')
os.chdir('/work/smyumeng/LULC/scripts/py/')

# *********************************************************************
# Loading data
# *********************************************************************

# Ground Truth Data
LC_io = rasterio.open('/work/smyumeng/LULC/data/20210921_groundtruth_xz.tif')
LC = np.float32( LC_io.read(1) )

# Fullstack sentinel data
fullstack_io = rasterio.open('/work/smyumeng/LULC/data/20210921_fullstack_xz.tif')
fullstack = np.float16( fullstack_io.read() )

# GLCM data

pca_glcm_io = rasterio.open('/work/smyumeng/LULC/data/20210921_PCA_GLCM_xz.tif')
pca_glcm = np.float16( pca_glcm_io.read() )


# Labdels for classification
axlabels = ['unclassified', 'Water', 'Trees', '', 'Flooded vegetation', 'Crops', '', 'Built Area','Bare ground', 'Snow/Ice', 'Clouds', 'Rangeland']



# *********************************************************************
# Color Map
# *********************************************************************
# Colormap

from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch
# from xml4lulc_functions import mem

Cmaplist = [(3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            (213/255,238/255,178/255,1), # Flooded vegetations
            (253/255,133/255,19/255,1), # crops
            (43/255,131/255,186/255,1), # Build area
            (206/255,224/255,196/255,1), # Bare Ground
            (34/255,139/255,34/255,1)]

RF_Cmaplist = [(1,1,1,1), # Nan
            (3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            # (213/255,238/255,178/255,1), 
            (253/255,133/255,19/255,1), # Flooded vegetations
            (43/255,131/255,186/255,1), # crops
            (206/255,224/255,196/255,1), # Build area 
            (34/255,139/255,34/255,1)] # Bare Ground

cmap=ListedColormap(Cmaplist)
legend_patch=[Patch(color=icolor,label=label) for icolor, label in zip(Cmaplist,axlabels)]

# plt.figure(figsize=(15,15),dpi=150)
# plt.imshow(LC, cmap=cmap)
# plt.legend(handles=legend_patch, loc='upper left')
# mem()

# *********************************************************************
# Color Map
# *********************************************************************

# LC: Ground Truch Data - Remove 'unclassified', 'Snow/Ice', 'Clouds' and 'Rangeland'

LC[LC == 0] = np.nan
LC[LC == 3] = np.nan
LC[LC == 6] = np.nan
LC[LC == 9] = np.nan
LC[LC == 10] = np.nan

# LC[LC > 0] -= 1
LC[LC > 3] -= 1
LC[LC > 5] -= 1
LC[LC > 7] -= 1
LC[LC > 7] -= 1

axlabels = ['Water', 'Forest', 'Flooded vegetation', 'Crops', 'Built Area','Bare ground', 'Rangeland']

gc.collect()
np.unique(LC)[:8]
# Return a new array of given shape and type, filled with ones.
kernel = np.ones( (2,2),np.uint8 )

# LC2: Remove ground truth data edge pixels
# LC = np.float32( LC_io.read(1) )
LC2 = np.zeros( np.shape(LC) )
for ii in range( 1, int(np.nanmax(LC))+1 ):
  single_class_layer = np.zeros( np.shape(LC) )
  single_class_layer[LC == ii] = ii
  single_class_layer = cv2.erode( single_class_layer, kernel, iterations = 1 )
  # 参数说明：src表示的是输入图片，kernel表示的是方框的大小，iteration表示迭代的次数
  # 腐蚀操作原理：存在一个kernel，比如(2, 2)，在图像中不断的平移，在这个9方框中，哪一种颜色所占的比重大，9个方格中将都是这种颜色
  LC2 += single_class_layer
LC2[LC2==0]=np.nan

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# transpose
# 求矩阵的转置
fullstack=np.transpose(fullstack,(1,2,0))

# normalize
# Min-Max Normalization 最小-最大值标准化

fullstack -= np.min(fullstack,axis=(0,1))
fullstack /= np.max(fullstack,axis=(0,1))

# set np.save() 路径
os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/')
np.save('fullStack_float16_normalized', fullstack)
# fullstack=np.float16( fullstack )

# *************************************************************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *************************************************************************
# fullstack = np.load('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/fullStack_float16_normalized.npy')

# 2d array
dime = np.shape(fullstack)
# fullstack_2d = np.float16( np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' ) )

# np.ravel() Return a contiguous flattened array. 让多维数组变成一维数组 
fullstack_2d = np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' )

gc.collect()


# *********************************************************************
# GLCM
# *********************************************************************

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# transpose
# 求矩阵的转置
pca_glcm=np.transpose(pca_glcm,(1,2,0))

# normalize
# Min-Max Normalization 最小-最大值标准化

pca_glcm -= np.min(pca_glcm,axis=(0,1))
pca_glcm /= np.max(pca_glcm,axis=(0,1))

# set np.save() 路径
os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/')
np.save('pca_glcm_float16_normalized_reproject', pca_glcm)

# *****************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *****************************
# pca_glcm = np.load('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/pca_glcm_float16_normalized_reproject.npy')

# 2d array
dime = np.shape(pca_glcm)
pca_glcm_2d = np.reshape( np.ravel(pca_glcm, order='F'), (dime[0]*dime[1], dime[2]), order='F' )
print(pca_glcm.shape)


gc.collect()

# *********************************************************************
# Spliting test and train samples
# *********************************************************************
gt_1d0 = np.int32( np.ravel(LC, order='F') ); gt_1d0[gt_1d0<1]=0
gt_1d = np.int32( np.ravel(LC2, order='F') ); gt_1d[gt_1d<1]=0

from this_functions import tr_te_sample

tr_samples=1000
# tr_samples=.20
val_samples=10000
tr_label, val_label, _, tr_idx, val_idx, _ = tr_te_sample( gt_1d, tr_samples, val_samples )

print('Training samples: ', str(len(tr_idx)))
print('Validation samples: ', str(len(val_idx)))
gc.collect()

np.unique(tr_label)

# *********************************************************************
# Remove some classes
# *********************************************************************

# Omitting label 3: Flooded vegetations

tr_idx = np.delete( tr_idx, np.where(tr_label==3) )
val_idx = np.delete( val_idx, np.where(val_label==3) )
tr_label = np.delete( tr_label, np.where(tr_label==3) )
val_label = np.delete( val_label, np.where(val_label==3) )
gt_1d[gt_1d==3]=0
gt_1d0[gt_1d0==3]=0

tr_label[tr_label>3] -= 1
val_label[val_label>3] -= 1
gt_1d[gt_1d>3] -= 1
gt_1d0[gt_1d0>3] -= 1

np.unique(tr_label)

# *********************************************************************
# Preprocessing
# *********************************************************************

train = fullstack_2d[tr_idx]
validation = fullstack_2d[val_idx]

gc.collect()

# *********************************************************************
# XGBoosted
# *********************************************************************
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from xgboost import XGBClassifier

le = preprocessing.LabelEncoder()
le = le.fit(tr_label)
le_tr_label = le.transform(tr_label)
print(le_tr_label)

le = le.fit(val_label)
le_val_label = le.transform(val_label)
print(le_val_label)


# plot learning curve of an xgboost model
# define the model
classifier_XG = XGBClassifier(n_estimators=400)

# define the datasets to evaluate each iteration
evalset = [(train, le_tr_label), (validation[:2000,:], le_val_label[:2000])]
# fit the model
classifier_XG.fit(train, le_tr_label, eval_metric='mlogloss', early_stopping_rounds=10, eval_set=evalset)
# evaluate performance
yhat = classifier_XG.predict(validation[:2000,:])

# retrieve performance metrics
score = accuracy_score(val_label[:2000], yhat)
print('Accuracy: %.3f' % score)
results = classifier_XG.evals_result()
# plot learning curves

lc_imgpath = '/work/smyumeng/LULC/result/XGBoosted/lc_imgpath.png'
plt.plot(results['validation_0']['mlogloss'], label='train')
plt.plot(results['validation_1']['mlogloss'], label='validation')
plt.legend(); 
plt.savefig(lc_imgpath)
#plt.show()

# Prediction
ytr_pred = classifier_XG.predict(train)
yval_pred = classifier_XG.predict(validation)

SCENARIOS = np.arange(8,9,1)

for scenario in SCENARIOS:
  '''''''''''''''''''''''''''''''  # define the model '''''''''''''''''''''''''''''''
  classifier_XG = XGBClassifier(n_estimators=400)
  es = 20
  if scenario == 1:
    # 1 - Just Sentinel temporal: 1-11
    evalset = [(train[:,:11], le_tr_label), (validation[:2000,:11], le_val_label[:2000])]# define the datasets to evaluate each iteration
    classifier_XG.fit( train[:,:11], le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )

  if scenario == 2:
    # 2 - Just Sentinel temporal & Veg: 1-16
    evalset = [(train[:,:17], le_tr_label), (validation[:2000,:17], le_val_label[:2000])]# define the datasets to evaluate each iteration
    classifier_XG.fit( train[:,:17], le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )
    
  if scenario == 3:
    # 3 - Just Sentinel temporal & Topo: 1-11 & 17-18
    evalset = [(np.hstack( [train[:,:11], train[:,17:20]] ), le_tr_label), (np.hstack( [validation[:2000,:11], validation[:2000,17:20]] ), le_val_label[:2000])]# define the datasets to evaluate each iteration
    classifier_XG.fit( np.hstack( [train[:,:11], train[:,17:20]] ) , le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )

  if scenario == 4:
    # 8 - Multi-temporal, Spectral, Veg, and Topo
    evalset = [(train, le_tr_label), (validation[:2000], le_val_label[:2000])]# define the datasets to evaluate each iteration
    classifier_XG.fit( train, le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )





  # GLCM Scenarios

  if scenario == 5:
    # 5 - Just GLCM
    evalset = [( pca_glcm_2d[val_idx][:2000], le_val_label[:2000] )]# define the datasets to evaluate each iteration
    classifier_XG.fit( pca_glcm_2d[tr_idx], le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )

  if scenario == 6:
    # 6 - GLCM + Multi-temporal and Spectral
    evalset = [(np.hstack( [validation[:2000,:11], pca_glcm_2d[val_idx][:2000]] ), le_val_label[:2000] )]# define the datasets to evaluate each iteration
    classifier_XG.fit( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ), le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )

  if scenario == 7:
    # 7 - GLCM + Multi-temporal, Spectral, and Veg
    evalset = [(np.hstack( [validation[:2000,:17], pca_glcm_2d[val_idx][:2000]] ), le_val_label[:2000] )]# define the datasets to evaluate each iteration
    classifier_XG.fit( np.hstack( [train[:,:17], pca_glcm_2d[tr_idx]] ), le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )
    
  if scenario == 8:
    # 8 - GLCM + Multi-temporal, Spectral, and Topography
    evalset = [(np.hstack( [validation[:2000,:11], validation[:2000,17:20], pca_glcm_2d[val_idx][:2000]] ), le_val_label[:2000] )]# define the datasets to evaluate each iteration
    classifier_XG.fit( np.hstack( [train[:,:11], train[:,17:20], pca_glcm_2d[tr_idx]] ) , le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )

  if scenario == 9:
    # 9 - GLCM + Multi-temporal, Spectral, Veg, and Topo
    evalset = [(np.hstack( [validation[:2000], pca_glcm_2d[val_idx][:2000]] ), le_val_label[:2000] )]# define the datasets to evaluate each iteration
    classifier_XG.fit( np.hstack( [train, pca_glcm_2d[tr_idx]] ), le_tr_label, eval_metric='mlogloss', early_stopping_rounds=es, eval_set=evalset )


  '''''''''''''''''''# Prediction for different layers of fullstack data (scenarios)'''''''''''''''''''



  if scenario == 1:
    # 1 - Just Sentinel temporal: 1-11
    ytr_pred = classifier_XG.predict(train[:,:11])
    yval_pred = classifier_XG.predict(validation[:,:11])

  if scenario == 2:
    # 2 - Just Sentinel temporal & Veg: 1-16
    ytr_pred = classifier_XG.predict(train[:,:17])
    yval_pred = classifier_XG.predict(validation[:,:17])

  if scenario == 3:
    # 3 - Just Sentinel temporal & Topo: 1-11 & 17-18
    ytr_pred = classifier_XG.predict(np.hstack( [train[:,:11], train[:,17:20]] ))
    yval_pred = classifier_XG.predict(np.hstack( [validation[:,:11], validation[:,17:20]] ))

  if scenario == 4:
    # 4 - Multi-temporal, Spectral, Topography, and Vegetation
    ytr_pred = classifier_XG.predict(train)
    yval_pred = classifier_XG.predict(validation)


  # GLCM Scenarios

  if scenario == 5:
    # 5 - Just GLCM
    ytr_pred = classifier_XG.predict( pca_glcm_2d[tr_idx] )
    yval_pred = classifier_XG.predict( pca_glcm_2d[val_idx] )

  if scenario == 6:
    # 6 - GLCM + Multi-temporal and Spectral
    ytr_pred = classifier_XG.predict( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_XG.predict( np.hstack( [validation[:,:11], pca_glcm_2d[val_idx]] ) )

  if scenario == 7:
    # 7 - GLCM + Multi-temporal, Spectral, and Veg
    ytr_pred = classifier_XG.predict( np.hstack( [train[:,:16], pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_XG.predict( np.hstack( [validation[:,:16], pca_glcm_2d[val_idx]] ) )
    
  if scenario == 8:
    # 8 - GLCM + Multi-temporal, Spectral, and Topo
    ytr_pred = classifier_XG.predict( np.hstack( [train[:,:11], train[:,16:18], pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_XG.predict( np.hstack( [validation[:,:11], validation[:,16:18], pca_glcm_2d[val_idx]] ) )

  if scenario == 9:
    # 9 - GLCM + Multi-temporal, Spectral, Veg, and Topo
    ytr_pred = classifier_XG.predict( np.hstack( [train, pca_glcm_2d[tr_idx]] ) )
    yval_pred = classifier_XG.predict( np.hstack( [validation, pca_glcm_2d[val_idx]] ) )




  '''''''''''''''''''''''''''''''''''# Validation results'''''''''''''''''''''''''''''''''''

  # axlabels = ['1-Agriculture','3-Deciduous Trees','5-NonVegetated Open Land','6-Water','7-Wetland', '8-High Intensity Urban', '9-Low Intensity Urban', '10-Industrial']
  axlabels = ['1-Water', '2-Forest', '3-Crops', '4-Built Area','5-Bare ground', '6-Rangeland']
  # axlabels = ['1-Agriculture','2-Artificial','3-Coniferous Trees','4-Deciduous Trees','5-Mixed Forest', '6-Vegetated Open Land','7-NonVegetated Open Land','8-Water','9-Wetland']

  print('Scenario: \n', scenario)
  CM_val = this_functions.confusion_mat(le_val_label, yval_pred, axlabels=axlabels, plot=True,cmap="gray_r")
  # np.save('/content/drive/MyDrive/LCLU-deep learning/Results/CM_XG_Scenario '+str(scenario), CM_val)

# *********************************************************************
# Full Classification Map - XGBoosted
# *********************************************************************