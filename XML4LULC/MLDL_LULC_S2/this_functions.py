import tifffile as tif
from PIL import Image
import rasterio
import matplotlib.pyplot as plt
import numpy as np
import os
import geopandas
from osgeo import ogr, gdal
from copy import deepcopy


# def mem():
#     import resource
#     # 这个resource库，只支持linux、mac，windows用不了
#     # print('Memory usage         : % 2.2f MB' % round(
#     #     resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0,1)
#     # )

#     # 此函数返回一个对象，该对象描述当前进程或其子进程消耗的资源，由 who 参数指定。 who 参数应使用下述 RUSAGE_* 常量之一指定。
#     return round( resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0,1 )
    
def coord2pixel(ds, xp, yp):
    # unravel GDAL affine transform parameters
    c, a, b, f, d, e = ds.get_transform()
    """Returns global coordinates to pixel center using base-0 raster index"""

    # 计算各元素的ceiling，对元素向上取整。
    col = int( np.ceil( ( b*yp - e*xp - b*d/2 + e*a/2 - b*f + e*c ) / (b*d - a*e) ) )
    row = int( np.ceil( ( a*yp - d*xp - a*e/2 + d*b/2 - a*f + d*c ) / (a*e - b*d) ) )
    return(row, col)

# #

def shape2tif(Shapefile_path, ref_image_path, OutputImage='shp2tiff.tif'):
    import ogr, gdal
    import subprocess
    # Open Shapefile
    Shapefile = ogr.Open(Shapefile_path)

    gdalformat = 'GTiff'
    datatype = gdal.GDT_Byte
    burnVal = 1  # value for the output image pixels
    # Get projection info from reference image
    Shapefile_layer = Shapefile.GetLayer()
    Image = gdal.Open(ref_image_path, gdal.GA_ReadOnly)
    # Rasterise
    print("Rasterising shapefile...")
    Output = gdal.GetDriverByName(gdalformat).Create(OutputImage, Image.RasterXSize, Image.RasterYSize, 1, datatype,
                                                     options=['COMPRESS=DEFLATE'])
    Output.SetProjection(Image.GetProjectionRef())
    Output.SetGeoTransform(Image.GetGeoTransform())
    # Write data to band 1
    Band = Output.GetRasterBand(1)
    Band.SetNoDataValue(0)
    gdal.RasterizeLayer(Output, [1], Shapefile_layer, burn_values=[burnVal])
    # Close datasets
    Band = None
    Output = None
    Image = None
    Shapefile = None
    # Build image overviews
    subprocess.call("gdaladdo --config COMPRESS_OVERVIEW DEFLATE " + OutputImage + " 2 4 8 16 32 64", shell=True)

# #

def generate_raster_GT(ref_image_path, GT_shapefile_path_mask, GT_shapefile_path_2, OutputImage_name='GT_image.tif'):
  import rasterio
  from rasterio.mask import mask
  import geopandas as gpd
  from shapely.geometry import mapping

  # Generate binary tiff from shapefile
  shape2tif(GT_shapefile_path_mask, ref_image_path, OutputImage='shp2tiff.tif')
  OutputImage='shp2tiff.tif'
  GT = rasterio.open(OutputImage).read(1)
  # extract the geometry in GeoJSON format
  shapefile = gpd.read_file(GT_shapefile_path_2)

  shapefile.set_crs(epsg=32633, inplace=True, allow_override=True)
  shapefile.to_crs(epsg=32633)

  geoms = shapefile.geometry.values # list of polygons

  # classes =shapefile.Class.values # polygons' class
  classes = shapefile.fclass.values # polygons' class
  # classes =shapefile.Id.values # polygons' class
  # classes =shapefile.fid.values # polygons' class
  # classes =shapefile.DN.values # polygons' class

  labels_name = np.unique(classes) # unique labels

  # colorizing the tiff_binary_shapefile
  print( 'Number of Polygons: ', len(geoms))

  for ii in range( 0, len(geoms) ):
    polygon = [mapping(geoms[ii])]
    # extract the raster values within the polygon 
    with rasterio.open(OutputImage) as src:
        out_image, out_transform = mask(src, polygon, crop=True)
        # print('label: ',label_number)
        # Pixel2Coord (in the cropped image)
        a, b, c, d, e, f = out_transform[0], out_transform[1], out_transform[2], out_transform[3], out_transform[4], out_transform[5]
        """Returns global coordinates to pixel edge using base-0 raster index"""
        col1=0;row1=0
        xp_topleft = a * col1 + b * row1 + a * 0.0 + b * 0.5 + c
        yp_topleft = d * col1 + e * row1 + d * 0.5 + e *0.0 + f # a, e: cell ground size in X and Y directions
        col2=out_image.shape[2]-1;row2=out_image.shape[1]-1
        xp_bottomright = a * col2 + b * row2 + a * 0.0 + b * 0.5 + c
        yp_bottomright = d * col2 + e * row2 + d * 0.5 + e *0.0 + f # a, e: cell ground size in X and Y directions

        # Coord2pixel (in the main image)
        r_topleft, c_topleft = coord2pixel(src, xp_topleft, yp_topleft)
        r_bottomright, c_bottomright = coord2pixel(src, xp_bottomright, yp_bottomright)
        # print('area: ',r_topleft,r_bottomright,c_topleft, c_bottomright)

        # colorizing
        GT[r_topleft:r_bottomright+1, c_topleft:c_bottomright+1]+=(label_number-1)*out_image[0]
  # For deleting a file:
  import os
  os.remove(OutputImage)
  
  return GT

# # 

def tr_te_sample( gt_1d, tr_samples, val_samples, mode='tr' ): # mode = 'tr' or 'val'
  np.random.seed(1)
  tr_idx = []; te_idx = []; val_idx = []
  tr_label = []; te_label = []; val_label = []
  for ii in range(1, max(gt_1d)+1):
    L = np.where(gt_1d==ii)[0]
    np.random.shuffle(L)
    if tr_samples>1:
      samples = deepcopy(tr_samples)
    else:
      samples = int( tr_samples*len(L) )
      
    v_samples = deepcopy(val_samples)

    tr_idx.append(L[:samples])
    val_idx.append(L[samples:samples+v_samples])
    te_idx.append(L[samples+v_samples:])
    #
    tr_label.append( ii*np.ones( len(L[:samples]) ) )
    val_label.append( ii*np.ones( len(L[samples:samples+v_samples]) ) ) 
    te_label.append( ii*np.ones( len(L[samples+v_samples:]) ) ) 
  #
  tr_label=np.uint8( np.hstack(tr_label) )
  val_label=np.uint8( np.hstack(val_label) )
  te_label=np.uint8( np.hstack(te_label) )
  #
  tr_idx=np.uint32( np.hstack(tr_idx) )
  val_idx=np.uint32( np.hstack(val_idx) )
  te_idx=np.uint32( np.hstack(te_idx) )
  #
  idx = np.arange(0, len(tr_label), 1); np.random.shuffle(idx)
  tr_label=tr_label[idx]; tr_idx=tr_idx[idx]
  #
  idx = np.arange(0, len(val_label), 1); np.random.shuffle(idx)
  val_label=val_label[idx]; val_idx=val_idx[idx]
  #
  idx = np.arange(0, len(te_label), 1); np.random.shuffle(idx)
  te_label=te_label[idx]; te_idx=te_idx[idx]

  return tr_label, val_label, te_label, tr_idx, val_idx, te_idx

# # 

def two_d (cube):
    d1, d2, d3 = cube.shape
    im_2d = np.reshape(np.ravel(cube, order='F'), (d1*d2, d3),order=1)
    # col  1 of band 1    col  1 of band 2     ...............     ...............    col  1 of band d3
    # col  2 of band 1    col  2 of band 2     ...............     ...............    col  2 of band d3
    # ................    ................     ...............     ...............    .................
    # ................    ................     ...............     ...............    .................
    # col d2 of band 1    col d2 of band 2     ...............     ...............    col d2 of band d3
    return im_2d

# #
# Normalize to [0, 1]
def normalize(im_2d):  # Map features value to [0, 1]
    d12, d3 = im_2d.shape
    band_max = np.max(im_2d, axis=0) * np.ones((d12, d3))  # maximum radiance in every band
    band_min = np.min(im_2d, axis=0) * np.ones((d12, d3))  # minimum radiance in every band

    #norm_2d = 2*(im_2d-band_max)/(band_max-band_min) + np.ones((d12, d3))  # norm = 2 * ( [x-max]/[max-min] ) +1
    norm_2d = np.float16((im_2d-band_min)/(band_max-band_min))  # norm =  [x-min]/[max-min] 
    return norm_2d


def confusion_mat(label, predicted, cm_path_name, axlabels, plot=True,cmap="gray_r"):
    #OA
    from sklearn.metrics import classification_report, confusion_matrix
    conf_mat = np.float32(confusion_matrix(label, predicted)) 
    OA = 100*np.trace(conf_mat)/len(predicted)
    print('Data Overall Accuracy: ', OA)
    #Kappa
    from sklearn.metrics import cohen_kappa_score
    Kappa = 100*cohen_kappa_score(label, predicted)
    print('Kappa score: ', Kappa)
    from sklearn.metrics import balanced_accuracy_score
    bal_OA = 100*balanced_accuracy_score(label, predicted)
    print('Data Balanced Overall Accuracy: ', bal_OA)
    #Normalized Confusion Mat: Recall
    conf_mat_re=100*conf_mat/np.expand_dims(np.sum(conf_mat,axis=1),1)
    #Normalized Confusion Mat: Precision
    conf_mat_pr=100*conf_mat/np.expand_dims(np.sum(conf_mat,axis=0),1)
    OPr = np.mean(np.diag(conf_mat_pr))
    print('Overall Precision: ', OPr)
    #Normalized Confusion Mat: f1-score
    conf_mat_f1=2*conf_mat_pr*conf_mat_re/(conf_mat_pr+conf_mat_re)
    OF1 = np.mean(np.diag(conf_mat_f1))
    print('Overall F1: ', OF1)
    # conf_mat_f1=(conf_mat/np.expand_dims(np.sum(conf_mat,axis=1),1)+conf_mat/np.expand_dims(np.sum(conf_mat,axis=0),1))/2
    conf_mat_f1[np.isnan(conf_mat_f1)==True]=0
    #Heat Map
    import seaborn as sn
    import pandas  as pd
    if plot==True:
      plt.figure(figsize = (15, 15.45), dpi=100, facecolor='w', edgecolor='k')
      plt.rcParams["font.family"] = "Times New Roman"
      # cmap = sn.cubehelix_palette(start=2, rot=0, dark=0, light=.9, reverse=True, as_cmap=True)
      # cmap="gray_r"
      # cmap="Greens"
      #1
      plt.subplot(2,2,1);plt.title('Confusion matrix: OA='+format( OA, '.4f' )+'; K='+format( Kappa, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat[ii,jj]<50:
            annots[-1] += ['']
          else:
            annots[-1] += [str(int(conf_mat[ii,jj]))]
      #
      axlabels_num = []
      for numx in range(1,len(axlabels)+1):
        axlabels_num+=[str(numx)]
      ax=sn.heatmap(conf_mat, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num,# linecolor='gray', # 
                 yticklabels=axlabels, cmap=cmap, fmt = '',linewidths=.0)# font size
      ax.set(xlabel='Predicted', ylabel='Actual');
      ax.axis('equal')
      # Frame
      lw=1
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #2
      plt.subplot(2,2,2);plt.title('F1: AF1='+format( OF1, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_f1[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_f1[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_f1, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num, 
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #3
      plt.subplot(2,2,3);plt.title('Recall (Normalized CM): AA='+format( bal_OA, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_re[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_re[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_re, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num,
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #4
      plt.subplot(2,2,4);plt.title('Precision: APr='+format( OPr, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_pr[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_pr[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_pr, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num, 
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      plt.savefig(cm_path_name)
      #plt.show()
    #Other Evaluation Metrics
    print('Data Evaluation:')
    print(classification_report(label, predicted)) 
    
    df = pd.DataFrame(data=[OA, Kappa, bal_OA, OPr, OF1],    # values
                              index=['OA', 'Kappa', 'BA', 'Pr', 'F1'],    # 1st column as index
                              columns=['Results'])  # 1st row as the column names
    # display(df)

    return conf_mat


def confusion_mat_dl(label, predicted, axlabels, plot=True,cmap="gray_r"):
    #OA
    from sklearn.metrics import classification_report, confusion_matrix
    conf_mat = np.float32(confusion_matrix(label, predicted)) 
    OA = 100*np.trace(conf_mat)/len(predicted)
    print('Data Overall Accuracy: ', OA)
    #Kappa
    from sklearn.metrics import cohen_kappa_score
    Kappa = 100*cohen_kappa_score(label, predicted)
    print('Kappa score: ', Kappa)
    from sklearn.metrics import balanced_accuracy_score
    bal_OA = 100*balanced_accuracy_score(label, predicted)
    print('Data Balanced Overall Accuracy: ', bal_OA)
    #Normalized Confusion Mat: Recall
    conf_mat_re=100*conf_mat/np.expand_dims(np.sum(conf_mat,axis=1),1)
    #Normalized Confusion Mat: Precision
    conf_mat_pr=100*conf_mat/np.expand_dims(np.sum(conf_mat,axis=0),1)
    OPr = np.mean(np.diag(conf_mat_pr))
    print('Overall Precision: ', OPr)
    #Normalized Confusion Mat: f1-score
    conf_mat_f1=2*conf_mat_pr*conf_mat_re/(conf_mat_pr+conf_mat_re)
    OF1 = np.mean(np.diag(conf_mat_f1))
    print('Overall F1: ', OF1)
    # conf_mat_f1=(conf_mat/np.expand_dims(np.sum(conf_mat,axis=1),1)+conf_mat/np.expand_dims(np.sum(conf_mat,axis=0),1))/2
    conf_mat_f1[np.isnan(conf_mat_f1)==True]=0
    #Heat Map
    import seaborn as sn
    import pandas  as pd
    if plot==True:
      plt.figure(figsize = (15, 15.45), dpi=100, facecolor='w', edgecolor='k')
      plt.rcParams["font.family"] = "Times New Roman"
      # cmap = sn.cubehelix_palette(start=2, rot=0, dark=0, light=.9, reverse=True, as_cmap=True)
      # cmap="gray_r"
      # cmap="Greens"
      #1
      plt.subplot(2,2,1);plt.title('Confusion matrix: OA='+format( OA, '.4f' )+'; K='+format( Kappa, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat[ii,jj]<50:
            annots[-1] += ['']
          else:
            annots[-1] += [str(int(conf_mat[ii,jj]))]
      #
      axlabels_num = []
      for numx in range(1,len(axlabels)+1):
        axlabels_num+=[str(numx)]
      ax=sn.heatmap(conf_mat, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num,# linecolor='gray', # 
                 yticklabels=axlabels, cmap=cmap, fmt = '',linewidths=.0)# font size
      ax.set(xlabel='Predicted', ylabel='Actual');
      ax.axis('equal')
      # Frame
      lw=1
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #2
      plt.subplot(2,2,2);plt.title('F1: AF1='+format( OF1, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_f1[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_f1[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_f1, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num, 
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #3
      plt.subplot(2,2,3);plt.title('Recall (Normalized CM): AA='+format( bal_OA, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_re[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_re[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_re, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num,
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #4
      plt.subplot(2,2,4);plt.title('Precision: APr='+format( OPr, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_pr[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_pr[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_pr, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num, 
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      plt.savefig('/work/smyumeng/LULC/result/test/cm_2.png')
      #plt.show()
    #Other Evaluation Metrics
    print('Data Evaluation:')
    print(classification_report(label, predicted)) 
    
    df = pd.DataFrame(data=[OA, Kappa, bal_OA, OPr, OF1],    # values
                              index=['OA', 'Kappa', 'BA', 'Pr', 'F1'],    # 1st column as index
                              columns=['Results'])  # 1st row as the column names
    # display(df)

    return conf_mat
