import tifffile as tif
from PIL import Image
import rasterio
import matplotlib.pyplot as plt
import numpy as np
import os
import geopandas
import ogr, gdal
from copy import deepcopy
from IPython import display
import gc
gc.enable()

import os

# os.chdir() 方法用于改变当前工作目录到指定的路径

# os.chdir('/content/drive/MyDrive/LCLU-deep learning')
os.chdir('D:/Projects/LULC_deep_learning')


### 


# def mem():
#     import resource
#     # 这个resource库，只支持linux、mac，windows用不了
#     # print('Memory usage         : % 2.2f MB' % round(
#     #     resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0,1)
#     # )

#     # 此函数返回一个对象，该对象描述当前进程或其子进程消耗的资源，由 who 参数指定。 who 参数应使用下述 RUSAGE_* 常量之一指定。
#     return round( resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0,1 )
    
def coord2pixel(ds, xp, yp):
    # unravel GDAL affine transform parameters
    c, a, b, f, d, e = ds.get_transform()
    """Returns global coordinates to pixel center using base-0 raster index"""

    # 计算各元素的ceiling，对元素向上取整。
    col = int( np.ceil( ( b*yp - e*xp - b*d/2 + e*a/2 - b*f + e*c ) / (b*d - a*e) ) )
    row = int( np.ceil( ( a*yp - d*xp - a*e/2 + d*b/2 - a*f + d*c ) / (a*e - b*d) ) )
    return(row, col)

# #

def shape2tif(Shapefile_path, ref_image_path, OutputImage='shp2tiff.tif'):
    import ogr, gdal
    import subprocess
    # Open Shapefile
    Shapefile = ogr.Open(Shapefile_path)

    gdalformat = 'GTiff'
    datatype = gdal.GDT_Byte
    burnVal = 1  # value for the output image pixels
    # Get projection info from reference image
    Shapefile_layer = Shapefile.GetLayer()
    Image = gdal.Open(ref_image_path, gdal.GA_ReadOnly)
    # Rasterise
    print("Rasterising shapefile...")
    Output = gdal.GetDriverByName(gdalformat).Create(OutputImage, Image.RasterXSize, Image.RasterYSize, 1, datatype,
                                                     options=['COMPRESS=DEFLATE'])
    Output.SetProjection(Image.GetProjectionRef())
    Output.SetGeoTransform(Image.GetGeoTransform())
    # Write data to band 1
    Band = Output.GetRasterBand(1)
    Band.SetNoDataValue(0)
    gdal.RasterizeLayer(Output, [1], Shapefile_layer, burn_values=[burnVal])
    # Close datasets
    Band = None
    Output = None
    Image = None
    Shapefile = None
    # Build image overviews
    subprocess.call("gdaladdo --config COMPRESS_OVERVIEW DEFLATE " + OutputImage + " 2 4 8 16 32 64", shell=True)

# #

def generate_raster_GT(ref_image_path, GT_shapefile_path_mask, GT_shapefile_path_2, OutputImage_name='GT_image.tif'):
  import rasterio
  from rasterio.mask import mask
  import geopandas as gpd
  from shapely.geometry import mapping

  # Generate binary tiff from shapefile
  shape2tif(GT_shapefile_path_mask, ref_image_path, OutputImage='shp2tiff.tif')
  OutputImage='shp2tiff.tif'
  GT = rasterio.open(OutputImage).read(1)
  # extract the geometry in GeoJSON format
  shapefile = gpd.read_file(GT_shapefile_path_2)

  shapefile.set_crs(epsg=32633, inplace=True, allow_override=True)
  shapefile.to_crs(epsg=32633)

  geoms = shapefile.geometry.values # list of polygons

  # classes =shapefile.Class.values # polygons' class
  classes = shapefile.fclass.values # polygons' class
  # classes =shapefile.Id.values # polygons' class
  # classes =shapefile.fid.values # polygons' class
  # classes =shapefile.DN.values # polygons' class

  labels_name = np.unique(classes) # unique labels

  # colorizing the tiff_binary_shapefile
  print( 'Number of Polygons: ', len(geoms))

  for ii in range( 0, len(geoms) ):
    polygon = [mapping(geoms[ii])]
    # extract the raster values within the polygon 
    with rasterio.open(OutputImage) as src:
        out_image, out_transform = mask(src, polygon, crop=True)
        label_number = np.where( labels_name == classes[ii] )[0][0]+1
        # print('label: ',label_number)
        # Pixel2Coord (in the cropped image)
        a, b, c, d, e, f = out_transform[0], out_transform[1], out_transform[2], out_transform[3], out_transform[4], out_transform[5]
        """Returns global coordinates to pixel edge using base-0 raster index"""
        col1=0;row1=0
        xp_topleft = a * col1 + b * row1 + a * 0.0 + b * 0.5 + c
        yp_topleft = d * col1 + e * row1 + d * 0.5 + e *0.0 + f # a, e: cell ground size in X and Y directions
        col2=out_image.shape[2]-1;row2=out_image.shape[1]-1
        xp_bottomright = a * col2 + b * row2 + a * 0.0 + b * 0.5 + c
        yp_bottomright = d * col2 + e * row2 + d * 0.5 + e *0.0 + f # a, e: cell ground size in X and Y directions

        # Coord2pixel (in the main image)
        r_topleft, c_topleft = coord2pixel(src, xp_topleft, yp_topleft)
        r_bottomright, c_bottomright = coord2pixel(src, xp_bottomright, yp_bottomright)
        # print('area: ',r_topleft,r_bottomright,c_topleft, c_bottomright)

        # colorizing
        GT[r_topleft:r_bottomright+1, c_topleft:c_bottomright+1]+=(label_number-1)*out_image[0]
  # For deleting a file:
  import os
  os.remove(OutputImage)
  
  return GT

# # 

def tr_te_sample( gt_1d, tr_samples, val_samples, mode='tr' ): # mode = 'tr' or 'val'
  np.random.seed(1)
  tr_idx = []; te_idx = []; val_idx = []
  tr_label = []; te_label = []; val_label = []
  for ii in range(1, max(gt_1d)+1):
    L = np.where(gt_1d==ii)[0]
    np.random.shuffle(L)
    if tr_samples>1:
      samples = deepcopy(tr_samples)
    else:
      samples = int( tr_samples*len(L) )
      
    v_samples = deepcopy(val_samples)

    tr_idx.append(L[:samples])
    val_idx.append(L[samples:samples+v_samples])
    te_idx.append(L[samples+v_samples:])
    #
    tr_label.append( ii*np.ones( len(L[:samples]) ) )
    val_label.append( ii*np.ones( len(L[samples:samples+v_samples]) ) ) 
    te_label.append( ii*np.ones( len(L[samples+v_samples:]) ) ) 
  #
  tr_label=np.uint8( np.hstack(tr_label) )
  val_label=np.uint8( np.hstack(val_label) )
  te_label=np.uint8( np.hstack(te_label) )
  #
  tr_idx=np.uint32( np.hstack(tr_idx) )
  val_idx=np.uint32( np.hstack(val_idx) )
  te_idx=np.uint32( np.hstack(te_idx) )
  #
  idx = np.arange(0, len(tr_label), 1); np.random.shuffle(idx)
  tr_label=tr_label[idx]; tr_idx=tr_idx[idx]
  #
  idx = np.arange(0, len(val_label), 1); np.random.shuffle(idx)
  val_label=val_label[idx]; val_idx=val_idx[idx]
  #
  idx = np.arange(0, len(te_label), 1); np.random.shuffle(idx)
  te_label=te_label[idx]; te_idx=te_idx[idx]

  return tr_label, val_label, te_label, tr_idx, val_idx, te_idx

# # 

def two_d (cube):
    d1, d2, d3 = cube.shape
    im_2d = np.reshape(np.ravel(cube, order='F'), (d1*d2, d3),order=1)
    # col  1 of band 1    col  1 of band 2     ...............     ...............    col  1 of band d3
    # col  2 of band 1    col  2 of band 2     ...............     ...............    col  2 of band d3
    # ................    ................     ...............     ...............    .................
    # ................    ................     ...............     ...............    .................
    # col d2 of band 1    col d2 of band 2     ...............     ...............    col d2 of band d3
    return im_2d

# #
# Normalize to [0, 1]
def normalize(im_2d):  # Map features value to [0, 1]
    d12, d3 = im_2d.shape
    band_max = np.max(im_2d, axis=0) * np.ones((d12, d3))  # maximum radiance in every band
    band_min = np.min(im_2d, axis=0) * np.ones((d12, d3))  # minimum radiance in every band

    #norm_2d = 2*(im_2d-band_max)/(band_max-band_min) + np.ones((d12, d3))  # norm = 2 * ( [x-max]/[max-min] ) +1
    norm_2d = np.float16((im_2d-band_min)/(band_max-band_min))  # norm =  [x-min]/[max-min] 
    return norm_2d


def confusion_mat(label, predicted, axlabels, plot=True,cmap="gray_r"):
    #OA
    from sklearn.metrics import classification_report, confusion_matrix
    conf_mat = np.float32(confusion_matrix(label, predicted)) 
    OA = 100*np.trace(conf_mat)/len(predicted)
    print('Data Overall Accuracy: ', OA)
    #Kappa
    from sklearn.metrics import cohen_kappa_score
    Kappa = 100*cohen_kappa_score(label, predicted)
    print('Kappa score: ', Kappa)
    from sklearn.metrics import balanced_accuracy_score
    bal_OA = 100*balanced_accuracy_score(label, predicted)
    print('Data Balanced Overall Accuracy: ', bal_OA)
    #Normalized Confusion Mat: Recall
    conf_mat_re=100*conf_mat/np.expand_dims(np.sum(conf_mat,axis=1),1)
    #Normalized Confusion Mat: Precision
    conf_mat_pr=100*conf_mat/np.expand_dims(np.sum(conf_mat,axis=0),1)
    OPr = np.mean(np.diag(conf_mat_pr))
    print('Overall Precision: ', OPr)
    #Normalized Confusion Mat: f1-score
    conf_mat_f1=2*conf_mat_pr*conf_mat_re/(conf_mat_pr+conf_mat_re)
    OF1 = np.mean(np.diag(conf_mat_f1))
    print('Overall F1: ', OF1)
    # conf_mat_f1=(conf_mat/np.expand_dims(np.sum(conf_mat,axis=1),1)+conf_mat/np.expand_dims(np.sum(conf_mat,axis=0),1))/2
    conf_mat_f1[np.isnan(conf_mat_f1)==True]=0
    #Heat Map
    import seaborn as sn
    import pandas  as pd
    if plot==True:
      plt.figure(figsize = (15, 15.45), dpi=100, facecolor='w', edgecolor='k')
      plt.rcParams["font.family"] = "Times New Roman"
      # cmap = sn.cubehelix_palette(start=2, rot=0, dark=0, light=.9, reverse=True, as_cmap=True)
      # cmap="gray_r"
      # cmap="Greens"
      #1
      plt.subplot(2,2,1);plt.title('Confusion matrix: OA='+format( OA, '.4f' )+'; K='+format( Kappa, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat[ii,jj]<50:
            annots[-1] += ['']
          else:
            annots[-1] += [str(int(conf_mat[ii,jj]))]
      #
      axlabels_num = []
      for numx in range(1,len(axlabels)+1):
        axlabels_num+=[str(numx)]
      ax=sn.heatmap(conf_mat, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num,# linecolor='gray', # 
                 yticklabels=axlabels, cmap=cmap, fmt = '',linewidths=.0)# font size
      ax.set(xlabel='Predicted', ylabel='Actual');
      ax.axis('equal')
      # Frame
      lw=1
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #2
      plt.subplot(2,2,2);plt.title('F1: AF1='+format( OF1, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_f1[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_f1[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_f1, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num, 
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #3
      plt.subplot(2,2,3);plt.title('Recall (Normalized CM): AA='+format( bal_OA, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_re[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_re[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_re, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num,
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      #4
      plt.subplot(2,2,4);plt.title('Precision: APr='+format( OPr, '.4f' ), fontsize=14)
      sn.set(font_scale=1)#for label size
      # Annotation
      annots = []
      for ii in range(len(axlabels)):
        annots.append([])
        for jj in range(len(axlabels)):
          if conf_mat_pr[ii,jj]<1:
            annots[-1] += ['']
          else:
            annots[-1] += [format( conf_mat_pr[ii,jj], '.2f' )]
      #
      ax=sn.heatmap(conf_mat_pr, annot=np.array(annots),annot_kws={"size": 12}, cbar=False, xticklabels=axlabels_num, yticklabels=axlabels_num, 
                    cmap=cmap, vmin=0, vmax=100, fmt = '',linewidths=.0)# font size
      ax.axis('equal')
      # Frame
      ax.axhline(y=0, color='k',linewidth=lw)
      ax.axhline(y=len(axlabels), color='k',linewidth=lw)
      ax.axvline(x=0, color='k',linewidth=lw)
      ax.axvline(x=len(axlabels), color='k',linewidth=lw)
      plt.show()
    #Other Evaluation Metrics
    print('Data Evaluation:')
    print(classification_report(label, predicted)) 
    
    df = pd.DataFrame(data=[OA, Kappa, bal_OA, OPr, OF1],    # values
                              index=['OA', 'Kappa', 'BA', 'Pr', 'F1'],    # 1st column as index
                              columns=['Results'])  # 1st row as the column names
    display(df)

    return conf_mat


LC_io = rasterio.open('D:/DATA/LULC/02_Training/01_test/clip_test_ground_truth.tif')
LC = np.float32( LC_io.read(1) )

# 11 labels
# axlabels = ['Agriculture','Coniferous Trees','Deciduous Trees','Mixed Forest',
#             'Vegetated Open Land','NonVegetated Open Land','Water','Wetland', 'High Intensity Urban', 'Low Intensity Urban', 'Industrial']

axlabels = ['unclassified', 'Water', 'Trees', '', 'Flooded vegetation', 'Crops', '', 'Built Area','Bare ground', 'Snow/Ice', 'Clouds', 'Rangeland']

# Remove 'unclassified', 'Snow/Ice', 'Clouds' and 'Rangeland'

LC[LC == 0] = np.nan
LC[LC == 3] = np.nan
LC[LC == 6] = np.nan
LC[LC == 9] = np.nan
LC[LC == 10] = np.nan

# LC[LC > 0] -= 1
LC[LC > 3] -= 1
LC[LC > 5] -= 1
LC[LC > 7] -= 1
LC[LC > 7] -= 1

axlabels = ['Water', 'Forest', 'Flooded vegetation', 'Crops', 'Built Area','Bare ground', 'Rangeland']

gc.collect()
np.unique(LC)[:8]


# Colormap

from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch
# from xml4lulc_functions import mem

Cmaplist = [(3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            (213/255,238/255,178/255,1), # Flooded vegetations
            (253/255,133/255,19/255,1), # crops
            (43/255,131/255,186/255,1), # Build area
            (206/255,224/255,196/255,1), # Bare Ground
            (34/255,139/255,34/255,1)]

cmap=ListedColormap(Cmaplist)
legend_patch=[Patch(color=icolor,label=label) for icolor, label in zip(Cmaplist,axlabels)]

plt.figure(figsize=(15,15),dpi=150)
plt.imshow(LC, cmap=cmap)
plt.legend(handles=legend_patch, loc='upper left')
# mem()

# opencv
import cv2
import numpy as np

# Return a new array of given shape and type, filled with ones.
kernel = np.ones( (2,2),np.uint8 )

# LC = np.float32( LC_io.read(1) )
LC2 = np.zeros( np.shape(LC) )
for ii in range( 1, int(np.nanmax(LC))+1 ):
  single_class_layer = np.zeros( np.shape(LC) )
  single_class_layer[LC == ii] = ii
  single_class_layer = cv2.erode( single_class_layer, kernel, iterations = 1 )
  # 参数说明：src表示的是输入图片，kernel表示的是方框的大小，iteration表示迭代的次数
  # 腐蚀操作原理：存在一个kernel，比如(2, 2)，在图像中不断的平移，在这个9方框中，哪一种颜色所占的比重大，9个方格中将都是这种颜色
  LC2 += single_class_layer
LC2[LC2==0]=np.nan


from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch

Cmaplist = [(3/255,20/255,176/255,1), # water
            (1/255,58/255,11/255,1), # Forest
            (213/255,238/255,178/255,1), # Flooded vegetations
            (253/255,133/255,19/255,1), # crops
            (43/255,131/255,186/255,1), # Build area
            (206/255,224/255,196/255,1), # Bare Ground
            (34/255,139/255,34/255,1)]
            
axlabels = ['Water', 'Forest', 'Flooded vegetation', 'Crops', 'Built Area','Bare ground', 'Rangeland']

cmap=ListedColormap(Cmaplist)
legend_patch=[Patch(color=icolor,label=label) for icolor, label in zip(Cmaplist,axlabels)]

plt.figure(figsize=(15,15),dpi=150)
plt.imshow(LC2, cmap=cmap)
plt.legend(handles=legend_patch, loc='upper left')
# mem()


from rasterio.windows import Window 

fullstack_io = rasterio.open('D:/DATA/LULC/01_Prepare/Sentinel2/06_test_dataset/test_S2A_MSIL2A_20210921T025551_N9999_R032_T50_20221119T225446_super_resolved_mosaic_stack_DEM_slope_collocation.tif')

fullstack = np.float16( fullstack_io.read() )

fullstack = np.load('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/fullStack_float16_normalized.npy')

gc.collect()


# 2d array
dime = np.shape(fullstack)
# fullstack_2d = np.float16( np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' ) )

# np.ravel() Return a contiguous flattened array. 让多维数组变成一维数组 
fullstack_2d = np.reshape( np.ravel(fullstack, order='F'), (dime[0]*dime[1], dime[2]), order='F' )

print(fullstack.shape)
print(fullstack_2d.shape)
gc.collect()

''' Displaying the RGB '''
dispp = np.float32( np.stack( [fullstack[:,:,2],
                               fullstack[:,:,1],
                               fullstack[:,:,0]], axis=2 ) )
meandisp = np.mean(dispp,axis=(0,1))
stdisp = np.std(dispp,axis=(0,1))
dispp = (dispp-meandisp)/(5*stdisp)
dispp[dispp>1]=1
dispp[dispp<-1]=-1
dispp +=1
dispp /=2

fig, ax=plt.subplots(nrows=1, ncols=1, figsize=(10,10),dpi=100)
ax.imshow(dispp);plt.axis(False)
# plt.savefig('/content/drive/MyDrive/LCLU-deep learning/Results/Maps/RGB.jpg', dpi=600)

''' Displaying the Topography '''

mask = LC+0
mask[mask>1]=1
dispp = np.float32(mask*fullstack[:,:,16])

plt.figure(figsize=(13,10),dpi=300)
plt.imshow(dispp,cmap='jet');plt.axis(False);# plt.colorbar(label='Elevation (m)')
# plt.savefig('/content/drive/MyDrive/LCLU-deep learning/Results/Maps/DEM_Jet.jpg', dpi=600)

''' Displaying the NDVI '''
mask = LC+0
mask[mask>1]=1
dispp = np.float32(mask*fullstack[:,:,13])

fig, ax=plt.subplots(nrows=1, ncols=1, figsize=(15,8),dpi=300)
ax.imshow(dispp,cmap='jet');plt.axis(False)
plt.savefig('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/Results/Maps/NDVI_Jet.jpg', dpi=600)


# 2 types of Layers' Visualization

# No.1
os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/fullStack')
layers_name = np.loadtxt("Layer_names.txt", delimiter=",",comments='str', dtype = '<U128',unpack=False)

fig = plt.subplots(figsize=(20, 20),dpi=400)
plt.subplots_adjust(hspace=.3)

subplot_r = 8
subplot_c = 8

ax=[]
mask = LC+0

mask[mask>1]=1
for ii in range(fullstack.shape[2]):
    ax.append( plt.subplot(subplot_r, subplot_c, ii+1) )
    layer_ii = mask*fullstack[:,:,ii]
    # layer_ii[layer_ii==0] = np.nan
    # plt.imshow(layer_ii, cmap='jet');plt.title( str(ii+1)+' - '+layers_name[ii][1:-1] )#; plt.grid(True)
    plt.imshow(layer_ii, cmap='jet');plt.title( str(ii+1)+' - '+layers_name[ii] )#; plt.grid(True)
    plt.axis('off')

# No.2

# Layers' Visualization

# fig = plt.subplots(figsize=(25, 25),dpi=200)
# plt.subplots_adjust(hspace=.3)


# for ii in range(fullstack.shape[2]):
#     ax.append( plt.subplot(subplot_r, subplot_c, ii+1) )
#     hist = mask*fullstack[:,:,ii]
#     hist=hist[np.isnan(hist)==False]
#     plt.hist(hist, bins=100);plt.title( str(ii+1)+' - '+layers_name[ii][1:-1] )#; plt.grid(True)
#     plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
#     # plt.axis('off')


pca_glcm_io = rasterio.open('D:/DATA/LULC/01_Prepare/Sentinel2/06_test_dataset/clip_test_PCA_GLCM_fullstack_reproject.tif')

pca_glcm = np.float16( pca_glcm_io.read() )

# fullstack=fullstack_io.read( window=Window.from_slices((r1, r2), (c1, c2)) )#clip

# *****************************
# 保存标准化的fullstack 为.npy文件，方便之后使用
# 将下列注释代码解注释
# *****************************

# # transpose
# # 求矩阵的转置
# pca_glcm=np.transpose(pca_glcm,(1,2,0))

# # normalize
# # Min-Max Normalization 最小-最大值标准化

# pca_glcm -= np.min(pca_glcm,axis=(0,1))
# pca_glcm /= np.max(pca_glcm,axis=(0,1))

# # set np.save() 路径
# os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/')
# np.save('pca_glcm_float16_normalized_reproject', pca_glcm)
# # fullstack=np.float16( fullstack )

# *****************************
# 读取标准化的fullstack 为.npy文件
# 注释下列代码
# *****************************
pca_glcm = np.load('C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/test/pca_glcm_float16_normalized_reproject.npy')
print(pca_glcm.shape)

# print(fullstack_2d.shape)

gc.collect()


# 2d array
dime = np.shape(pca_glcm)
pca_glcm_2d = np.reshape( np.ravel(pca_glcm, order='F'), (dime[0]*dime[1], dime[2]), order='F' )
print(pca_glcm.shape)
gc.collect()




pca_glcm_2d = np.nan_to_num(pca_glcm_2d)

print(np.isnan(pca_glcm_2d).any())
print(np.isinf(pca_glcm_2d).any())
print(np.isfinite(pca_glcm_2d).all())


# GLCM hist Layers' Visualization

os.chdir(r'C:/Users/mengy/Documents/VS_workspace/lulc-mapping-in-xuzhou/XML4LULC/fullStack')
pca_glcm_layers_name = np.loadtxt("pca_glcm_layers_name.txt", delimiter=",",comments='str', dtype = '<U128',unpack=False)


fig = plt.subplots(figsize=(25, 35), dpi=500)
plt.subplots_adjust(hspace=.5, wspace=.3)

subplot_r = 16
subplot_c = 10

ax=[]
mask = pca_glcm[:,:,0]+0
mask[mask>1]=1
for ii in range(pca_glcm.shape[2]):
    ax.append( plt.subplot(subplot_r, subplot_c, ii+1) )
    hist = mask*pca_glcm[:,:,ii]
    hist=hist[np.isnan(hist)==False]
    plt.hist(hist, bins=100);plt.title( str(ii+1)+' - '+pca_glcm_layers_name[ii][0:],{'fontsize':8} )#; plt.grid(True)
    plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    # plt.axis('off')


# GLCM Layers' Visualization

fig = plt.subplots(figsize=(25, 35), dpi=200)
plt.subplots_adjust(hspace=.3)

subplot_r = 16
subplot_c = 10

ax=[]
mask = pca_glcm[:,:,0]+0

for ii in range(pca_glcm.shape[2]):
    ax.append( plt.subplot(subplot_r, subplot_c, ii+1) )
    layer_ii = mask*pca_glcm[:,:,ii]
    # GLCM[ii][GLCM[ii]==np.min(GLCM)]=np.nanmean( GLCM[ii][GLCM[ii]!=np.nanmin(GLCM[ii])] )
    plt.imshow( np.float32(layer_ii), cmap='jet' );plt.title( str(ii+1)+' - '+pca_glcm_layers_name[ii][0:] )#; plt.grid(True)
    gc.collect()
    plt.axis('off')



gt_1d0 = np.int32( np.ravel(LC, order='F') ); gt_1d0[gt_1d0<1]=0
gt_1d = np.int32( np.ravel(LC2, order='F') ); gt_1d[gt_1d<1]=0


# from xml4lulc_functions import tr_te_sample

tr_samples=1000
# tr_samples=.20
val_samples=10000
tr_label, val_label, _, tr_idx, val_idx, _ = tr_te_sample( gt_1d, tr_samples, val_samples )

print('Training samples: ', str(len(tr_idx)))
print('Validation samples: ', str(len(val_idx)))
gc.collect()

np.unique(tr_label)

train = fullstack_2d[tr_idx]
validation = fullstack_2d[val_idx]
print(train.shape)
gc.collect()


# RF
"""CLASSIFIER"""
from sklearn.ensemble import RandomForestClassifier

classifier_RF = RandomForestClassifier(n_estimators=500,max_depth=None)
# classifier_RF.fit(train, tr_label)

scenario = 9

# For different layers (scenarios)
# from sklearn.ensemble import RandomForestClassifier

if scenario == 1:
  # 1 - Just Sentinel temporal: 1-11
  classifier_RF = RandomForestClassifier(n_estimators=600,max_depth=60)
  classifier_RF.fit( train[:,:11], tr_label )

if scenario == 2:
  # 2 - Just Sentinel temporal & Veg: 1-16
  classifier_RF = RandomForestClassifier(n_estimators=600)#,max_depth=60)
  classifier_RF.fit( train[:,:16], tr_label )
  
if scenario == 3:
  # 3 - Just Sentinel temporal & Topo: 1-11 & 17-18
  classifier_RF = RandomForestClassifier(n_estimators=600)#,max_depth=60)
  classifier_RF.fit( np.hstack( [train[:,:11], train[:,16:18]] ) , tr_label)
  
if scenario == 4:
  # 4 - Just Sentinel temporal & Veg & Topo: 1-18
  classifier_RF = RandomForestClassifier(n_estimators=600)#,max_depth=60)
  classifier_RF.fit( train, tr_label )

pca_glcm_2d = np.nan_to_num(pca_glcm_2d)


# GLCM Scenarios
# from sklearn.ensemble import RandomForestClassifier

# Fix error: ValueError: Input contains NaN, infinity or a value too large for dtype('float32').

train = np.nan_to_num(train)

if scenario == 5:
  # 5 - Just GLCM
  classifier_RF = RandomForestClassifier(n_estimators=600,max_depth=60)
  classifier_RF.fit( pca_glcm_2d[tr_idx], tr_label )

if scenario == 6:
  # 6 - GLCM + Multi-temporal and Spectral
  classifier_RF = RandomForestClassifier(n_estimators=600,max_depth=60)
  classifier_RF.fit( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ), tr_label )

if scenario == 7:
  # 7 - GLCM + Multi-temporal, Spectral, and Vegetation
  classifier_RF = RandomForestClassifier(n_estimators=600)#,max_depth=60)
  classifier_RF.fit( np.hstack( [train[:,:16], pca_glcm_2d[tr_idx]] ), tr_label )
  
if scenario == 8:
  # 8 - GLCM + Multi-temporal, Spectral, and Topography
  classifier_RF = RandomForestClassifier(n_estimators=600)#,max_depth=60)
  classifier_RF.fit( np.hstack( [train[:,:11], train[:,16:18], pca_glcm_2d[tr_idx]] ) , tr_label)
  
if scenario == 9:
  # 9 - GLCM + Multi-temporal, Spectral, Vegetation, and Topography
  classifier_RF = RandomForestClassifier(n_estimators=600)#,max_depth=60)
  classifier_RF.fit( np.hstack( [train, pca_glcm_2d[tr_idx]] ), tr_label )
  
# Prediction for different layers of fullstack data (scenarios)

if scenario == 1:
  # 1 - Just Sentinel temporal: 1-11
  ytr_pred = classifier_RF.predict(train[:,:11])
  yval_pred = classifier_RF.predict(validation[:,:11])

if scenario == 2:
  # 2 - Just Sentinel temporal & Veg: 1-16
  ytr_pred = classifier_RF.predict(train[:,:16])
  yval_pred = classifier_RF.predict(validation[:,:16])

if scenario == 3:
  # 3 - Just Sentinel temporal & Topo: 1-11 & 17-18
  ytr_pred = classifier_RF.predict(np.hstack( [train[:,:11], train[:,16:18]] ))
  yval_pred = classifier_RF.predict(np.hstack( [validation[:,:11], validation[:,16:18]] ))

if scenario == 4:
  # 4 - Just Sentinel temporal & Veg & Topo: 1-18
  ytr_pred = classifier_RF.predict(np.hstack(train))
  yval_pred = classifier_RF.predict(np.hstack(validation))

  

# GLCM Scenarios
pca_glcm_2d = np.nan_to_num(pca_glcm_2d)
validation = np.nan_to_num(validation)

if scenario == 5:
  # 5 - Just GLCM
  ytr_pred = classifier_RF.predict( pca_glcm_2d[tr_idx] )
  yval_pred = classifier_RF.predict( pca_glcm_2d[val_idx] )

if scenario == 6:
  # 6 - GLCM + Multi-temporal and Spectral
  ytr_pred = classifier_RF.predict( np.hstack( [train[:,:11], pca_glcm_2d[tr_idx]] ) )
  yval_pred = classifier_RF.predict( np.hstack( [validation[:,:11], pca_glcm_2d[val_idx]] ) )

if scenario == 7:
  # 7 - GLCM + Multi-temporal, Spectral, and Vegetation
  ytr_pred = classifier_RF.predict( np.hstack( [train[:,:16], pca_glcm_2d[tr_idx]] ) )
  yval_pred = classifier_RF.predict( np.hstack( [validation[:,:16], pca_glcm_2d[val_idx]] ) )
  
if scenario == 8:
  # 8 - GLCM + Multi-temporal, Spectral, and Topography
  ytr_pred = classifier_RF.predict( np.hstack( [train[:,:11], train[:,16:18], pca_glcm_2d[tr_idx]] ) )
  yval_pred = classifier_RF.predict( np.hstack( [validation[:,:11], validation[:,16:18], pca_glcm_2d[val_idx]] ) )

if scenario == 9:
  # 9 - GLCM + Multi-temporal, Spectral, Vegetation, and Topography
  ytr_pred = classifier_RF.predict( np.hstack( [train, pca_glcm_2d[tr_idx]] ) )
  yval_pred = classifier_RF.predict( np.hstack( [validation, pca_glcm_2d[val_idx]] ) )

# Training results

# from xml4lulc_functions import confusion_mat

# axlabels = ['Water', 'Forest', 'Flooded vegetation', 'Crops', 'Built Area','Bare ground', 'Rangeland']

CM_tr = confusion_mat(tr_label, ytr_pred, axlabels=axlabels, plot=True,cmap="gray_r")

print(CM_tr)