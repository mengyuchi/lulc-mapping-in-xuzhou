# In Lituratures

## Multi-spectral bands

1. Blue (**B2**), Green (**B3**), Red (**B4**), Red Edge 1 (**B5**), Red Edge 2 (**B6**), Red Edge 3 (**B7**), near infrared (NIR) (**B8**), Red Edge 4 (**B8A**), short-wave infrared (SWIR) 1 (**B11**), and SWIR 2 (**B12**) + **NDVI** + **DEM** in [Land Use/Land Cover Mapping Using Multitemporal Sentinel-2 Imagery and Four Classification Methods—A Case Study from Dak Nong, Vietnam](https://www.mdpi.com/2072-4292/12/9/1367)

2. S-2 bands: **2-8A**, **11**, **12** + **NDVI**, **NDBI**, **GNDVI**) + **DEM**, **slope** in [Land Use and Land Cover Mapping Using Sentinel-2, Landsat-8 Satellite Images, and Google Earth Engine: A Comparison of Two Composition Methods](https://www.mdpi.com/2072-4292/14/9/1977)

3. 10 m spatial resolution (**B2** Blue, **B3** Green, **B4** Red, **B8** NIR bands) and 20 m spatial resolution (**B5–B7** and **B8A** Vegetation red edge and **B11-B12** SWIR bands) + **SRTM V3 DEM** in [Random Forest Classification of Land Use, Land-Use Change and Forestry (LULUCF) Using Sentinel-2 Data—A Case Study of Czechia](https://www.mdpi.com/2072-4292/14/5/1189)

4. 10 bands (**2–8**, **8A**, **11** and **12**) were composited with 20 m resolution as used for classification in [Comparison of Random Forest, k-Nearest Neighbor, and Support Vector Machine Classifiers for Land Cover Classification Using Sentinel-2 Imagery](https://www.mdpi.com/1424-8220/18/1/18)

# Training and Validation Datasets

1. > Within the study area, 11 LULC classes were distinguished: (1) dense evergreen broadleaved forest (the forest has been slightly impacted); (2) open evergreen broadleaved forest (the forest has been moderately to heavily disturbed); (3) semi-evergreen forest (the forest that consists of a mixture of evergreen and deciduous dipterocarp tree species); (4) deciduous dipterocarp forest; (5) plantation forest; (6) mature rubber (≥3 years old); (7) perennial industrial plants; (8) croplands (annual crop land); (9) residential area; (10) water surface; and (11) other lands including, but not limited to, other types of grassland, shrubs, bare land, and abandoned land. 
    > 
    > good results using sample data from a combination of sources including field investigations, very fine spatial resolution Google Earth imagery, current Landsat and Sentinel imagery, and other sources such as maps.
    >
    > A similar approach was used for this study for which three sets of sample data were acquired in 2017 and 2018: (1) field observations for a purposive sample of size 232; (2) visual interpretations of fine and very fine resolution imagery from sources that included Google Earth for a purposive sample size of 214; and (3) visual interpretations of fine and very fine resolution imagery from sources that included Google Earth and Sentinel- 2A imagery for a simple random sample size of 800. in [Land Use/Land Cover Mapping Using Multitemporal Sentinel-2 Imagery and Four Classification Methods—A Case Study from Dak Nong, Vietnam](https://www.mdpi.com/2072-4292/12/9/1367)

2. > Generally, a high classification accuracy of the remotely sensed datasets requires large sets of training and validation samples. Therefore, a second step was that of generating a high number of training and validation samples to properly manage the issues of insufficient sample sizes and large numbers of dimensions [51,52]. Based on the above, an RF classifier was used to produce LULC maps and to evaluate the classification accuracy by a set of metrics. Since the accurate mapping of LULC classes based on machine learning methods requires a sufficient number of training samples [53], a visual inspection of high-resolution satellite imagery is a typical method used to extract training and validation samples [54,55]. In this study, a number of 3800 ground polygon samples (33,530 pixels) were defined based on a random distribution within LULC classes which included artificial land, cropland, woodland, grassland, bare land, and water bodies; this was done by a visual interpretation of the Google Earth highresolution satellite imagery. Therefore, we used this proportion to collect our samples in each LULC class and there was an imbalance between them. All the samples were divided into training (60%) and validation (40%) subsets in [Land Use and Land Cover Mapping Using Sentinel-2, Landsat-8 Satellite Images, and Google Earth Engine: A Comparison of Two Composition Methods](https://www.mdpi.com/2072-4292/14/9/1977).

3. > The Copernicus CLC (Corine Land Cover) 2018 datebase provided wihtin GEE platform and the ZM 10 map data and LPIS for year 2018 were used for the creation of training and validation datasets. Historical orthophotos from 2017, 2018 and 2019 and historical imageries in Google Earth Pro software were used to verify training polygons and validation points. Google Earth Pro provides imagery with very high resolution - Maxar satellite imagery with up to 0.3m spatial reslution (from 2015 to 2021). in [Random Forest Classification of Land Use, Land-Use Change and Forestry (LULUCF) Using Sentinel-2 Data—A Case Study of Czechia](https://www.mdpi.com/2072-4292/14/5/1189)

4. > The training data (training and testing samples) was collected based on the manual interpretation of the original Sentinel-2 data and high-resolution imagery available from Google Earth. 
    > 
    > To collect training sample data, the create polygon tool in the ArcGIS 10.5 toolbox was used to create 135 polygons for each land cover class. Due to the different polygon sizes, the number of pixels for each land cover class also differed. in [Comparison of Random Forest, k-Nearest Neighbor, and Support Vector Machine Classifiers for Land Cover Classification Using Sentinel-2 Imagery](https://www.mdpi.com/1424-8220/18/1/18)

## Classes

1. > Within the study area, 11 LULC classes were distinguished: (1) **dense evergreen broadleaved forest** (the forest has been slightly impacted); (2) **open evergreen broadleaved forest** (the forest has been moderately to heavily disturbed); (3) **semi-evergreen forest** (the forest that consists of a mixture of evergreen and deciduous dipterocarp tree species); (4) **deciduous dipterocarp forest**; (5) **plantation forest**; (6) **mature rubber** (≥3 years old); (7) perennial industrial plants; (8) **croplands** (annual crop land); (9) **residential area**; (10) **water surface**; and (11) **other lands** including, but not limited to, other types of grassland, shrubs, bare land, and abandoned land. in [Land Use/Land Cover Mapping Using Multitemporal Sentinel-2 Imagery and Four Classification Methods—A Case Study from Dak Nong, Vietnam](https://www.mdpi.com/2072-4292/12/9/1367)

2. In this study, a number of 3800 ground polygon samples (33,530 pixels) were defined based on a random distribution within LULC classes which included **artificial land**, **cropland**, **woodland**, **grassland**, **bare land**, and **water bodies**; this was done by a visual interpretation of the Google Earth highresolution satellite imagery. in [Land Use and Land Cover Mapping Using Sentinel-2, Landsat-8 Satellite Images, and Google Earth Engine: A Comparison of Two Composition Methods](https://www.mdpi.com/2072-4292/14/9/1977).

3. > The first basic methodological step was the creation of the classification nomenclature. The classification nomenclature follows the LULUCF regulations [15], which distinguish and report the status and development of areas of the following classes: **Forest Land**, **Cropland**, **Grassland**, **Wetlands**, **Settlements** and **Other Land**. Within the area of interest, the following classes were defined. in [Random Forest Classification of Land Use, Land-Use Change and Forestry (LULUCF) Using Sentinel-2 Data—A Case Study of Czechia](https://www.mdpi.com/2072-4292/14/5/1189)

4. > To collect training sample data, the create polygon tool in the ArcGIS 10.5 toolbox was used to create 135 polygons for each land cover class. Due to the different polygon sizes, the number of pixels for each land cover class also differed. **Residential**, **Impervious surface**, **Agriculture**, **bare land**, **forest** and **water**. in [Comparison of Random Forest, k-Nearest Neighbor, and Support Vector Machine Classifiers for Land Cover Classification Using Sentinel-2 Imagery](https://www.mdpi.com/1424-8220/18/1/18)

5. Land-cover types: 
    > We refer to Chinese Land Use Classification Criteria (GB/T21010-2017) to determine a hierarchical category system. In the large-scale classification set of GID, 5 major categories are annotated: **built-up**, **farmland**, **forest**, **meadow**, and **water**, which are pixel-level labeled with five different colors: red, green, cyan, yellow, and blue, respectively. Areas not belonging to the above five categories and clutter regions are labeled as **background**, which is represented using black color. The fine land-cover classification set is made up of 15 sub-categories: paddy field, irrigated land, dry cropland, garden land, arbor forest, shrub land, natural meadow, artificial meadow, industrial land, urban residential, rural residential, traffic land, river, lake, and pond. Its training set contains 2,000 patches per class, and validation images are labeled in pixel level.

    

# Dataset used in my project

## Stacked dataset

1. Sentinel-2A
    + 10m: **B2 B3 B4 B8**
    + 20m: **SRB5 SRB6 SRB7 SRB8A SRB9 SRB11 SRB12**

2. SRTM V3 DEM
    + Extent of `select and stack` dataset
        - southBound	34.01983866595984	ascii			
        - northBound	34.58781619824366	ascii			
        - eastBound	117.7167699067148	ascii			
        - westBound	116.81001605948157	ascii	
    + In ArcGIS Pro 
        - `mosaic to New Raster` 
        - Extent same as layer `xuzhou_center_dissolve`	

3. slope

4. Indexs
    + NDVI
    + MNDWI
    + REPI
    + BI2
    + [NDBI = (SWIR - NIR) / (SWIR + NIR)](https://pro.arcgis.com/en/pro-app/latest/help/analysis/raster-functions/band-arithmetic-function.htm) 
        - [Sentinel2A](https://gisgeography.com/sentinel-2-bands-combinations/#:~:text=Sentinel%202%20Bands%201%20Its%20blue%20%28B2%29%2C%20green,cirrus%20band%20%28B10%29%20have%20a%2060-meter%20pixel%20size.) SWIR: B11, NIR: B8A (in SNAP: (SRB11 - SRB8A) / (SRB11 + SRB8A))
        - [Landsat 8](https://gisrsstudy.com/landsat-8/#:~:text=Landsat%208%20Spacecraft%20Overview%201%20Altitude%3A%20705%20km,X-band%20frequency%E2%80%93384%20Mbps%2C%20S-band%20frequency-%20260.92%20Mbps%20%E6%9B%B4%E5%A4%9A%E9%A1%B9%E7%9B%AE) SWIR:B6, NIR:B5


## Process in ArcGIS

Using SRTM DEM to generate slope in Xuzhou.

## Process in SNAP

+ Prepare stacked dataset with all Sentinel bands, indexs and DEM.

When stack sentinel bands with DEM, we use `collocation` to porcesss.

+ Subset test area 

> Band Merge is not the correct operator to bring two products together. I recommend the Collocation tool[1].

## Training Dataset

Google Earth Pro + Sentinel2 + Land Use Map

Create training dataset in QGIS.

Land-cover types:
    - Build-up (red)
    - Farmland (green)
    - Forest (cyan)
    - Meadow (yellow)
    - Water (blue)
    - Other land (black)



### QGIS plugins

1. [HCMGIS](https://plugins.qgis.org/plugins/HCMGIS/)

> HCMGIS - Basemaps, Download OpenData, Batch Converter, VN-2000 Projections, Geometry Processing and Field Calculation Utilities

+ Add `OpenStreetMap`, `Google Maps`, `Bing Virtual Earth` etc. 

+ Original Sentinel-2A b4 b3 b2 display in SNAP (or find a way to display in QGIS?)

+ Create vertor shape files

2. [QuickMapServices](https://blog.csdn.net/x572722344/article/details/108121725)

    + `Bing Satellite`: <VintageStart>2011-11-12</VintageStart>
    + `Google Satellite`: 2022-09-05

## Create Land Type shapefile

1. Create `New Shapefile Layer`

    + Build-up (255,0,0)
        - Industrial land
        - Urban residential
        - Rural restdential
        - Traffic land

    + Farmland (0,255,0)
        - Paddy field
        - Irrigated land
        - Dry cropland

    + Forest (0,255,255)
        - Garden land
        - Arbor forest
        - Shrub land

    + Meadow (255,255,0)
        - Natural meadow
        - Artificial meadow

    + Water (0,0,255)
        - River
        - Lake
        - Pond

2. Reproject

`Vector` -> `Data Management Tools` -> `Reproject Layer`

![Reproject Layer](reproject_layer.png)

## Build model for Random Forest







[1]: https://forum.step.esa.int/t/error-nodeid-bandmerge-product-sourceproduct-1-is-not-compatibal-to-master-product/22549/2