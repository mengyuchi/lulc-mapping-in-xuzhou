import rasterio
import matplotlib

import random
import math
import itertools

from rasterio.plot import adjust_band
import matplotlib.pyplot as plt
from rasterio.plot import reshape_as_raster, reshape_as_image
from rasterio.plot import show
from rasterio.windows import Window
import rasterio.features
import rasterio.warp
import rasterio.mask

import numpy as np

from pyproj import Proj, transform
from tqdm import tqdm
from shapely.geometry import Polygon

from sklearn import neighbors, datasets
from sklearn.metrics import precision_score, accuracy_score, recall_score, f1_score

import keras
from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, BatchNormalization
from keras.callbacks import ModelCheckpoint

from functions import merge_classes
from functions import tile_generator
from functions import gen_balanced_pixel_locations
from functions import plot_confusion_matrix

from dict import class_names

# % matplotlib inline

# change to sentinel_path
sentinel_path = 'D:/DATA/LULC/01_Prepare/Sentinel2/06_test_dataset/test_S2A_MSIL2A_20210921T025551_N9999_R032_T50_20221119T225446_super_resolved_mosaic_stack_DEM_slope_collocation.tif'

# change to graoung_truth_path
labels_path = 'D:/DATA/LULC/02_Training/01_test/clip_test_ground_truth.tif'


# Open our raster dataset
sentinel_dataset = rasterio.open(sentinel_path)
labels_dataset = rasterio.open(labels_path)

# labels dataset
sentinel_datasets = [sentinel_dataset]
landsat_image = sentinel_dataset.read()
labels_image = merge_classes(labels_dataset.read())

print(landsat_image.shape)

# landsat_image.shape = (18, 1625, 2521)

# generate training pixels
train_pixels = gen_balanced_pixel_locations([sentinel_dataset], train_count=200, label_dataset=labels_dataset, merge=True)

# 1*1 tiles

im_batch = None
label_batch = None

sample_size = 200
train_count = 100

count = 0
for (im, label) in tile_generator(sentinel_datasets, labels_dataset, 1, 1, train_pixels, sample_size):
    if count > 0:
        break
    print('Batch Shape')
    print(im.shape)
    print('Label Shape')
    print(label.shape)
    print('----')
    count += 1
    im_batch = im
    label_batch = label

im_batch_reshaped = im_batch.reshape(sample_size,18)

# im_batch.shape = (200,1,1,18)
# im_batch_reshaped.shape = (200,18)

X_train = im_batch_reshaped[:train_count]
X_val = im_batch_reshaped[train_count:]
y_train = np.argmax(label_batch, axis=1)[:train_count]
y_val = np.argmax(label_batch, axis=1)[train_count:]


# X_val.shape = (100,18)

print(X_val.shape)

# ===================
# 
#  Run KNN
#
# =================== 
  

n_neighbors = 20

clf = neighbors.KNeighborsClassifier(n_neighbors, weights='distance')
clf.fit(X_train, y_train)

print('Accuracy: {accuracy:.2f}%'.format(accuracy = clf.score(X_val, y_val)*100))

pred_index = clf.predict(X_val)

# Plot non-normalized confusion matrix
plot_confusion_matrix(y_val, pred_index, classes=np.array(list(class_names)),
class_dict=class_names)

# Plot normalized confusion matrix
_ = plot_confusion_matrix(y_val, pred_index, classes=np.array(list(class_names)),class_dict=class_names,normalize=True)

plt.savefig('KNN.png')

print("Accuracy: {accuracy:.2f}%".format(accuracy = accuracy_score(y_val, pred_index)*100)) 
print("Precision: {precision:.2f}%".format(precision = precision_score(y_val, pred_index, average='weighted')*100))
print("Recall: {recall:.2f}%".format(recall = recall_score(y_val, pred_index, average='weighted')*100))
print("F1 score: {f1:.2f}%".format(f1 = f1_score(y_val, pred_index, average='weighted')*100))

# ===================
# 
#  Run Random Forest
#
# =================== 

from sklearn.ensemble import RandomForestClassifier

# Initialize our model with 500 trees
rf = RandomForestClassifier(n_estimators=500, 
                            oob_score=True)

# Fit our model to training data
rf = rf.fit(X_train, y_train)

print('Our OOB prediction of accuracy is: {oob:.2f}%'.format(oob=rf.oob_score_*100))

print('Acccuracy: {accuracy:.2f}%'.format(accuracy = rf.score(X_val, y_val)*100))

pred_index = rf.predict(X_val)

# Plot non-normalized confusion matrix
plot_confusion_matrix(y_val, pred_index, classes=np.array(list(class_names)), class_dict=class_names)

# Plot normalized confusion matrix
_ = plot_confusion_matrix(y_val, pred_index, classes=np.array(list(class_names)), class_dict=class_names, normalize=True)

plt.savefig('rf.png')

print("Accuracy: {accuracy:.2f}%".format(accuracy = accuracy_score(y_val, pred_index)*100)) 
print("Precision: {precision:.2f}%".format(precision = precision_score(y_val, pred_index, average='weighted')*100))
print("Recall: {recall:.2f}%".format(recall = recall_score(y_val, pred_index, average='weighted')*100))
print("F1 score: {f1:.2f}%".format(f1 = f1_score(y_val, pred_index, average='weighted')*100))

# ===================
# 
#  Run CNN
#
# =================== 

batch_size = 25
epochs = 50
num_classes = len(class_names)

# input image dimensions
tile_side = 32
img_rows, img_cols = tile_side, tile_side
img_bands = sentinel_datasets[0].count

input_shape = (img_rows, img_cols, img_bands)
print(input_shape)

model = Sequential()

model.add(Conv2D(32, (3, 3), padding='same', input_shape=input_shape))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3), padding='same'))
model.add(BatchNormalization())
model.add(Activation('relu'))

model.add(Conv2D(64, (3, 3), padding='same'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(128, (3, 3), padding='same'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(256, (3, 3), padding='same'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(128))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Dropout(0.25))

model.add(Dense(128))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Dropout(0.25))


model.add(Dense(num_classes))
model.add(Activation('softmax'))

print("\nModel architecture:\n")
# model.summary()

metrics=['accuracy']

model.compile(loss='categorical_crossentropy', metrics=metrics)

# print("Model compiled")

# Divide data into training and validation

train_to_val_ratio = 0.8
train_px = train_pixels[:int(len(train_pixels)*train_to_val_ratio)]
val_px = train_pixels[int(len(train_pixels)*train_to_val_ratio):]
print("# Training samples: {n_training} \n# Validation samples: {n_val}".
      format(n_training=len(train_px),n_val=len(val_px)))


history = model.fit_generator(generator=tile_generator(sentinel_datasets, 
                                                       labels_dataset, 
                                                       tile_side, tile_side, 
                                                       train_px, 
                                                       batch_size, 
                                                       merge=True), 

steps_per_epoch=len(train_px) // batch_size, epochs=epochs, verbose=1,

validation_data=tile_generator( sentinel_datasets,
                                labels_dataset,
                                tile_side, tile_side,
                                val_px,
                                batch_size,
                                merge=True),

validation_steps=len(val_px) // batch_size)

# Display of training and validation curves
# plt.figure(figsize=(1.62*7,7))
# plt.plot(history.history['accuracy'])
# plt.plot(history.history['val_accuracy'])
# plt.title('Model Accuracy')
# plt.ylabel('Accuracy (%)')
# plt.xlabel('Epoch')
# _ = plt.legend(['Train', 'Test'], loc='upper left') 


# Check out testing accuracy based on a confusion metrix

predictions = model.predict_generator(generator=tile_generator(sentinel_datasets, labels_dataset, tile_side, tile_side, val_px, batch_size, merge=True), 
                        steps=len(val_px) // batch_size,
                         verbose=1)

eval_generator = tile_generator(sentinel_datasets, labels_dataset, tile_side, tile_side, val_px, batch_size=1, merge=True)

labels = np.empty(predictions.shape)
count = 0
while count < len(labels):
    image_b, label_b = next(eval_generator)
    labels[count] = label_b
    count += 1
    
label_index = np.argmax(labels, axis=1)     
pred_index = np.argmax(predictions, axis=1)

np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
plot_confusion_matrix(label_index, pred_index, classes=np.array(list(class_names)),
                      class_dict=class_names)

# Plot normalized confusion matrix
_ = plot_confusion_matrix(label_index, pred_index, classes=np.array(list(class_names)),
                      class_dict=class_names,
                      normalize=True)

plt.savefig('cnn.png')

print("Accuracy: {accuracy:.2f}%".format(accuracy = accuracy_score(label_index, pred_index)*100)) 
print("Precision: {precision:.2f}%".format(precision = precision_score(label_index, pred_index, average='weighted')*100))
print("Recall: {recall:.2f}%".format(recall = recall_score(label_index, pred_index, average='weighted')*100))
print("F1 score: {f1:.2f}%".format(f1 = f1_score(label_index, pred_index, average='weighted')*100))

# Check out training accuracy based on a confusion matrix

predictions = model.predict_generator(generator=tile_generator(sentinel_datasets, labels_dataset, tile_side, tile_side, train_px, batch_size, merge=True), 
                        steps=len(train_px) // batch_size,
                         verbose=1)

eval_generator = tile_generator(sentinel_datasets, labels_dataset, tile_side, tile_side, train_px, batch_size=1, merge=True)

labels = np.empty(predictions.shape)
count = 0
while count < len(labels):
    image_b, label_b = next(eval_generator)
    labels[count] = label_b
    count += 1
    
label_index = np.argmax(labels, axis=1)     
pred_index = np.argmax(predictions, axis=1)

np.set_printoptions(precision=2)

# Plot non-normalized confusion matrix
_ = plot_confusion_matrix(label_index, pred_index, 
                          classes=np.array(list(class_names)),
                          class_dict=class_names)

# Plot normalized confusion matrix
_ = plot_confusion_matrix(label_index, pred_index, 
                          classes=np.array(list(class_names)),
                          class_dict=class_names,
                          normalize=True)

from sklearn.metrics import precision_score, accuracy_score, recall_score, f1_score
print("Accuracy: {accuracy:.2f}%".format(accuracy = accuracy_score(label_index, pred_index)*100)) 
print("Precision: {precision:.2f}%".format(precision = precision_score(label_index, pred_index, average='weighted')*100))
print("Recall: {recall:.2f}%".format(recall = recall_score(label_index, pred_index, average='weighted')*100))
print("F1 score: {f1:.2f}%".format(f1 = f1_score(label_index, pred_index, average='weighted')*100))