class_names = dict((
    (0, 'unclassified'),
    (1, 'Water'),
    (2, 'Trees'),
    (4, 'Flooded vegetation'),
    (5, 'Crops'),
    (7, 'Built Area'),
    (8, 'Bare ground'),
    (9, 'Snow/Ice'),
    (10, 'Clouds'),
    (10, 'Rangeland'),
))
