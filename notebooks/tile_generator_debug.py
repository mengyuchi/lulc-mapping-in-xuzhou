class_names = dict((
    (0, 'unclassified'),
    (1, 'Water'),
    (2, 'Trees'),
    (4, 'Flooded vegetation'),
    (5, 'Crops'),
    (7, 'Built Area'),
    (8, 'Bare ground'),
    (9, 'Snow/Ice'),
    (10, 'Clouds'),
    (10, 'Rangeland'),
))

import rasterio

# change to sentinel_path
l8_path = 'D:/DATA/LULC/01_Prepare/Sentinel2/06_test_dataset/test_S2A_MSIL2A_20210921T025551_N9999_R032_T50_20221119T225446_super_resolved_mosaic_stack_DEM_slope_collocation.tif'

# change to graoung_truth_path
labels_path = 'D:/DATA/LULC/02_Training/01_test/clip_test_ground_truth.tif'

import random
import math
import itertools

from rasterio.plot import adjust_band
import matplotlib.pyplot as plt
from rasterio.plot import reshape_as_raster, reshape_as_image
from rasterio.plot import show
from rasterio.windows import Window
import rasterio.features
import rasterio.warp
import rasterio.mask

from pyproj import Proj, transform
from tqdm import tqdm
from shapely.geometry import Polygon

def gen_balanced_pixel_locations(image_datasets, train_count, label_dataset, merge=False):
    ### this function pulls out a train_count + val_count number of random pixels from a list of raster datasets
    ### and returns a list of training pixel locations and image indices 
    ### and a list of validation pixel locations and indices
    
    label_proj = Proj(label_dataset.crs)    
    train_pixels = []
    
    train_count_per_dataset = math.ceil(train_count / len(image_datasets)) # 返回向上取整的整数
    print("train_count_per_dataset:",train_count_per_dataset)
    for index, image_dataset in enumerate(image_datasets):
        # how many points from each class
        points_per_class = train_count_per_dataset // len(np.unique(merge_classes(label_dataset.read())))
        print("points_per_class:",points_per_class)
        # get landsat boundaries in this image
        # create approx dataset mask in geographic coords
        # this fcn maps pixel locations in (row, col) coordinates to (x, y) spatial positions
        raster_points = image_dataset.transform * (0, 0), image_dataset.transform * (image_dataset.width, 0), image_dataset.transform * (image_dataset.width, image_dataset.height), image_dataset.transform * (0, image_dataset.height)
        print("raster_points:",raster_points)
        l8_proj = Proj(image_dataset.crs)
        new_raster_points = []
        # convert the raster bounds from landsat into label crs
        for x,y in raster_points:
            x,y = transform(l8_proj,label_proj,x,y)
            # convert from crs into row, col in label image coords
            row, col = label_dataset.index(x, y)
            # don't forget row, col is actually y, x so need to swap it when we append
            new_raster_points.append((col, row))
        # turn this into a polygon
        raster_poly = Polygon(new_raster_points)
        # Window.from_slices((row_start, row_stop), (col_start, col_stop))
        masked_label_image = label_dataset.read(window=Window.from_slices((int(raster_poly.bounds[1]), int(raster_poly.bounds[3])), (int(raster_poly.bounds[0]), int(raster_poly.bounds[2]))))
        if merge:
            masked_label_image = merge_classes(masked_label_image)
        # loop for each class
        all_points_per_image = []
        # for cls in np.unique(merge_classes(labels_image)):
        progression_bar = tqdm(np.unique(merge_classes(label_dataset.read())))
        for cls in (progression_bar):
            progression_bar.set_description("Processing « %s »" % str(class_names[int(cls)]))
            cls = int(cls)
            # mask the label subset image to each class
            # pull out the indicies where the mask is true
            rows,cols = np.where(masked_label_image[0] == cls)
            all_locations = list(zip(rows,cols))
            # shuffle all locations
            random.shuffle(all_locations)
            # now convert to landsat image crs
            # TODO need to time this to see if it is slow, can probably optimize
            l8_points = []
            # TODO Will probably need to catch this for classes smaller than the ideal points per class
            if len(all_locations)!=0:
                for r,c in all_locations[:points_per_class]:
                    # convert label row and col into label geographic space
                    x,y = label_dataset.xy(r+raster_poly.bounds[1],c+raster_poly.bounds[0])
                    # go from label projection into landsat projection
                    x,y = transform(label_proj, l8_proj,x,y)
                    # convert from landsat geographic space into row col
                    r,c = image_dataset.index(x,y)
                    l8_points.append((r,c))
                all_points_per_image += l8_points

        dataset_index_list = [index] * len(all_points_per_image)
        dataset_pixels = list(zip(all_points_per_image, dataset_index_list))
        train_pixels += dataset_pixels
        
    random.shuffle(train_pixels)
    return (train_pixels)

def tile_generator(l8_image_datasets, label_dataset, tile_height, tile_width, pixel_locations, batch_size, merge=False):
    ### this is a keras compatible data generator which generates data and labels on the fly 
    ### from a set of pixel locations, a list of image datasets, and a label dataset
     
    c = r = 0
    i = 0
    
    label_proj = Proj(label_dataset.crs)

    # assuming all images have the same num of bands
    l8_band_count = l8_image_datasets[0].count  
    band_count = l8_band_count
    class_count = len(class_names)
    buffer = math.ceil(tile_height / 2) # 返回大于等于的最大整数
  
    while True:
        image_batch = np.zeros((batch_size, tile_height, tile_width, band_count)) # take one off because we don't want the QA band
        label_batch = np.zeros((batch_size, class_count)) # The numpy.zeros() function returns a new array of given shape and type, with zeros.
        b = 0
        while b < batch_size:
            # if we're at the end  of the data just restart
            if i >= len(pixel_locations):
                i=0
            r, c = pixel_locations[i][0]
            dataset_index = pixel_locations[i][1]
            i += 1
            tile = l8_image_datasets[dataset_index].read(list(np.arange(1, l8_band_count+1)), window=Window(c-buffer, r-buffer, tile_width, tile_height))
            if tile.size == 0:
                pass
            elif np.amax(tile) == 0: # don't include if it is part of the image with no pixels
                pass
            elif np.isnan(tile).any() == True or -9999 in tile: 
                # we don't want tiles containing nan or -999 this comes from edges
                # this also takes a while and is inefficient
                pass
            elif tile.shape != (l8_band_count, tile_width, tile_height):
                #print('wrong shape')
                #print(tile.shape)
                # somehow we're randomly getting tiles without the correct dimensions
                pass
            #elif np.isin(tile[7,:,:], [352, 368, 392, 416, 432, 480, 840, 864, 880, 904, 928, 944, 1352]).any() == True:
                # make sure pixel doesn't contain clouds
                # this is probably pretty inefficient but only checking width x height for each tile
                # read more here: https://prd-wret.s3-us-west-2.amazonaws.com/assets/palladium/production/s3fs-public/atoms/files/LSDS-1873_US_Landsat_ARD_DFCB_0.pdf
                #print('Found some cloud.')
                #print(tile[7,:,:])
                #pass
            else:                
                # taking off the QA band
                #tile = tile[0:7]
                # reshape from raster format to image format and standardize according to image wide stats
                reshaped_tile = (reshape_as_image(tile)  - 982.5) / 1076.5

                ### get label data
                # find gps of that pixel within the image
                (x, y) = l8_image_datasets[dataset_index].xy(r, c)

                # convert the point we're sampling from to the same projection as the label dataset if necessary
                if l8_proj != label_proj:
                    x,y = transform(l8_proj,label_proj,x,y)

                # reference gps in label_image
                row, col = label_dataset.index(x,y)

                # find label
                # label image could be huge so we need this to just get a single position
                window = ((row, row+1), (col, col+1))
                data = merge_classes(label_dataset.read(1, window=window, masked=False, boundless=True))
                label = data[0,0]
                # if this label is part of the unclassified area then ignore
                #if label == 0 or np.isnan(label).any() == True:
                    #pass
                #else:                   
                    # add label to the batch in a one hot encoding style
                label_batch[b][label] = 1
                image_batch[b] = reshaped_tile
                b += 1
        yield (image_batch, label_batch)


def merge_classes(y):     
    y[y == 1] = 0
    class_names[0] = "Water"

    y[y == 2] = 1
    class_names[1] = "Forest"

    y[y == 4] = 2
    y[y == 5] = 2
    class_names[2] = "Farmland"
    
    y[y == 7] = 3
    class_names[3] = "Build_up"
    
    y[y == 8] = 4
    class_names[4] = "Bare_ground"

    y[y == 11] = 5
    class_names[5] = "Meadow"
    return(y)

import rasterio
import matplotlib.pyplot as plt
import numpy as np


# Open our raster dataset
landsat_dataset = rasterio.open(l8_path)


labels_dataset = rasterio.open(labels_path)
# we're merging here just to limit the number of classes we're working with
labels_image = merge_classes(labels_dataset.read())
labels_image.shape

colors = dict((
    #(0, (245,245,245, 255)), # Background
    (0, (0,0,255)), # Water
    (1, (0,255,255)), # Forest
    (2, (0,255,0)), # Farmland
    (3, (255,0,0)), # Build_up
    (4, (172, 177, 68)), # Bare_ground
    (5, (255,255,0)), # Meadow
))

n = int(np.max(labels_image)) + 1


# Put 0 - 255 as float 0 - 1
for k in colors:
    v = colors[k]
    _v = [_v / 255.0 for _v in v]
    colors[k] = _v
    
index_false_colors = [colors[key] for key in range(0, n)]

cmap_false_colors = plt.matplotlib.colors.ListedColormap(index_false_colors, 'Classification', n)

# Now show the class map next to the RGB image

fig, ax = plt.subplots(1,2, figsize=(3*10,10))

import matplotlib.patches as mpatches
patches =[mpatches.Patch(color=cmap_false_colors.colors[class_id],label=class_names[class_id]) 
          for class_id in range(len(cmap_false_colors.colors))]


train_pixels = gen_balanced_pixel_locations([landsat_dataset], train_count=3000, 
                                            label_dataset=labels_dataset, merge=True)

# the code block below generates a ton of warnings so we'll supress them for now
import warnings
warnings.filterwarnings("ignore")

landsat_datasets = [landsat_dataset]
# generate the training and validation pixel locations
all_labels = []
label_locations = []
progression_bar = tqdm(train_pixels)
for index,pixel in enumerate(progression_bar):
    progression_bar.set_description("Processing data point: %s" % str(index))

    # row, col location in landsat
    r,c = pixel[0]
    ds_index = pixel[1]
    l8_proj = Proj(landsat_datasets[ds_index].crs)
    label_proj = Proj(labels_dataset.crs)
    
    # geographic location in landsat
    x,y = landsat_datasets[ds_index].xy(r,c)
    # go from label projection into landsat projection
    x,y = transform(l8_proj, label_proj ,x,y)
    # get row and col location in label
    r,c = labels_dataset.index(x,y)
    
    label_locations.append([r,c])
    
    # format (bands, height, width)
    window = ((r, r+1), (c, c+1))
    data = merge_classes(labels_dataset.read(1, window=window, masked=False, boundless=True))
    all_labels.append(data[0,0])
    
label_locations = np.array(label_locations)

unique, counts = np.unique(np.array(all_labels), return_counts=True)
dict(zip(unique, counts))

im_batch = None

count = 0
for (im, label) in tile_generator(landsat_datasets, labels_dataset, 128, 128, train_pixels, 10):
    if count > 3:
        break
    print('Image')
    print(im.shape)
    print('Label')
    print(label.shape)
    print('----')
    count += 1
    im_batch = im
    label_batch = label

